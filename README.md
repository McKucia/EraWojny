<div align="center"> 

# War of Ages
<br />

## Login, Lobby list
<img src="/Assets/Textures/ReadmeImages/logo.gif" width="600">

## Buy units
<img src="/Assets/Textures/ReadmeImages/units.gif" width="600">

## Upgrade base, fight
<img src="/Assets/Textures/ReadmeImages/fight.gif" width="600">

## Buy upgrades
<img src="/Assets/Textures/ReadmeImages/upgrades.gif" width="600">

## Buy turrets to protect base
<img src="/Assets/Textures/ReadmeImages/turrets.gif" width="600">
</div>