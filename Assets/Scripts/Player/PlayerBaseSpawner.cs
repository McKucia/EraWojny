using UnityEngine;
using FishNet.Object;
using System.Collections.Generic;
using System;

public class PlayerBaseSpawner : NetworkBehaviour
{
    [SerializeField] Camera playerCamera;
    [SerializeField] List<NetworkObject> basePrefabs;
    [SerializeField] PlayerManager playerManager;
    [SerializeField] PlayerTurretSpawner turretSpawner;
    [SerializeField] Drag turretDrag;
    [SerializeField] MainCanvas mainCanvas;

    Base currentBase;
    NetworkObject currentBaseObject;
    int currentBaseIndex = 0; // for owner

    public override void OnStartClient()
    {
        base.OnStartClient();
        if (!base.IsOwner)
        {
            GetComponent<PlayerBaseSpawner>().enabled = false;
            return;
        }

        InitialSpawn();
    }

    void InitialSpawn()
    {
        Transform spawnPoint;
        if (base.IsHost)
            spawnPoint = GameObject.FindGameObjectWithTag("SpawnPointHost").transform;
        else
            spawnPoint = GameObject.FindGameObjectWithTag("SpawnPointClient").transform;

        transform.position = spawnPoint.position;
        transform.rotation = spawnPoint.rotation;

        SpawnBase(basePrefabs[currentBaseIndex], transform, currentBaseIndex);
    }

    public Base GetCurrentBase()
    {
        return currentBase;
    }

    public void UpgradeBase()
    {
        if (!base.IsOwner) return;
        currentBaseIndex++;
        DespawnObject(currentBaseObject);
        SpawnBase(basePrefabs[currentBaseIndex], transform, currentBaseIndex);
    }

    [ServerRpc]
    public void SpawnBase(NetworkObject obj, Transform player, int level)
    {
        NetworkObject nob = Instantiate<NetworkObject>(obj, player.position + obj.transform.localPosition, player.transform.rotation);
        var basee = nob.gameObject.GetComponent<Base>();
        basee.PlayerCamera = playerCamera;
        basee.PlayerManager = playerManager;
        basee.Level = level;
        turretDrag.currentBase = basee;
        currentBaseObject = nob;

        ServerManager.Spawn(nob, base.Owner);

        currentBase = basee;

        SetSpawnedObject(nob, level);
    }

    [ObserversRpc]
    public void SetSpawnedObject(NetworkObject spawned, int level)
    {
        TerrainUpdater.Instance.UpdateTerrain(level);
        var basee = spawned.gameObject.GetComponent<Base>();
        basee.PlayerCamera = playerCamera;
        basee.PlayerManager = playerManager;
        basee.Level = level;
        currentBaseObject = spawned;

        turretDrag.currentBase = basee;
        currentBase = basee;
    }

    [ServerRpc(RequireOwnership = false)]
    public void DespawnObject(NetworkObject obj)
    {
        ServerManager.Despawn(obj);
    }
}
