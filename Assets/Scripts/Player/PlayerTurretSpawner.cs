using UnityEngine;
using FishNet.Object;
using UnitySceneManager = UnityEngine.SceneManagement.SceneManager;
using System.Collections;
using FishNet.Demo.AdditiveScenes;

public class PlayerTurretSpawner : NetworkBehaviour
{
    [SerializeField] PlayerManager playerManager;

    public override void OnStartClient()
    {
        base.OnStartClient();
        if (!base.IsOwner)
            GetComponent<PlayerTurretSpawner>().enabled = false;
    }

    public void SpawnTurret(NetworkObject turret, NetworkObject parent)
    {
        SpawnObject(turret, parent, transform);
        Debug.Log(transform.rotation);
    }

    [ServerRpc]
    public void SpawnObject(NetworkObject obj, NetworkObject parent, Transform player)
    {
        NetworkObject nob = Instantiate<NetworkObject>(obj, Vector3.zero, player.rotation);
        nob.SetParent(parent);
        UnitySceneManager.MoveGameObjectToScene(nob.gameObject, gameObject.scene);
        ServerManager.Spawn(nob, base.Owner);
        
        SetSpawnedObject(nob);
    }

    [ObserversRpc]
    public void SetSpawnedObject(NetworkObject spawned)
    {
        UnitySceneManager.MoveGameObjectToScene(spawned.gameObject, gameObject.scene);
    }

    [ServerRpc(RequireOwnership = false)]
    public void DespawnObject(NetworkObject obj)
    {
        ServerManager.Despawn(obj);
    }
}
