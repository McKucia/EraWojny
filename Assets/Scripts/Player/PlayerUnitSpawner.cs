using UnityEngine;
using FishNet.Object;
using UnitySceneManager = UnityEngine.SceneManagement.SceneManager;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class PlayerUnitSpawner : NetworkBehaviour
{
    [SerializeField] Camera playerCamera;
    [SerializeField] PlayerManager playerManager;
    [SerializeField] TimerSlider timerSlider;
    [SerializeField] UnitsQueue unitsQueue;

    List<NetworkObject> unitsInQueue;
    List<NetworkObject> unitsReadyToSpawn;
    bool waitingToSpawn = false;
    int maxUnitsInQueue;
    float unitInsideTimer;
    const float unitInsideSafeTime = 2f;

    public override void OnStartClient()
    {
        base.OnStartClient();
        if (!base.IsOwner)
            GetComponent<PlayerUnitSpawner>().enabled = false;

        maxUnitsInQueue = unitsQueue.Toggles.Count + 1;
        unitsInQueue = new();
        unitsReadyToSpawn = new();
    }

    // true if can spawn
    public bool SpawnInfantry(NetworkObject unit)
    {
        if (!base.IsOwner) return false;

        if (unitsInQueue.Count + 1 > maxUnitsInQueue)
            return false;

        unitsInQueue.Add(unit);
        unitsQueue.UpdateQueue(unitsInQueue.Count - 1);

        return true;
    }

    void Update()
    {
        if(!base.IsOwner) return;

        if(unitsInQueue.Count > 0 && !waitingToSpawn)
        {
            waitingToSpawn = true;
            timerSlider.StartTimer(3f);
            StartCoroutine(SpawnObjectDelay(3f));
        }

        unitInsideTimer -= Time.deltaTime;
        if (unitsReadyToSpawn.Count > 0 && unitInsideTimer <= 0f)
        {
            if (!playerManager.GetCurrentBase().UnitInside())
            {
                var nextUnit = unitsReadyToSpawn.First();
                SpawnObject(nextUnit, transform);
                unitInsideTimer = unitInsideSafeTime;
                unitsReadyToSpawn.Remove(nextUnit);
            }
        }
    }

    IEnumerator SpawnObjectDelay(float seconds)
    {
        yield return new WaitForSecondsRealtime(seconds);
        var nextUnit = unitsInQueue.First();

        if (playerManager.GetCurrentBase().UnitInside())
            unitsReadyToSpawn.Add(nextUnit);
        else
        {
            if (unitsReadyToSpawn.Count <= 0)
                SpawnObject(nextUnit, transform);
            else
                unitsReadyToSpawn.Add(nextUnit);
        }

        unitsInQueue.Remove(nextUnit);
        unitsQueue.UpdateQueue(unitsInQueue.Count - 1);
        waitingToSpawn = false;
    }

    [ServerRpc]
    public void SpawnObject(NetworkObject obj, Transform player)
    {
        NetworkObject nob = Instantiate<NetworkObject>(obj, player.position + player.forward, player.transform.rotation);
        var unit = nob.gameObject.GetComponent<Unit>();
        unit.PlayerCamera = playerCamera;
        unit.PlayerManager = playerManager;
        var unitProperties = playerManager.UnitsProperties[playerManager.GetLevel()][unit.unitType.Value];
        unit.SetProperties(unitProperties.damageMin, unitProperties.damageMax, unitProperties.hp, unitProperties.movementSpeed);

        UnitySceneManager.MoveGameObjectToScene(nob.gameObject, gameObject.scene);

        ServerManager.Spawn(nob, base.Owner);

        SetSpawnedObject(nob);
    }

    [ObserversRpc]
    public void SetSpawnedObject(NetworkObject spawned )
    {
        UnitySceneManager.MoveGameObjectToScene(spawned.gameObject, gameObject.scene);
        var unit = spawned.gameObject.GetComponent<Unit>();
        unit.PlayerCamera = playerCamera;
        unit.PlayerManager = playerManager;

        var unitProperties = playerManager.UnitsProperties[playerManager.GetLevel()][unit.unitType.Value];
        unit.SetProperties(unitProperties.damageMin, unitProperties.damageMax, unitProperties.hp, unitProperties.movementSpeed);
    }

    [ServerRpc(RequireOwnership = false)]
    public void DespawnObject(NetworkObject obj)
    {
        ServerManager.Despawn(obj);
    }
}
