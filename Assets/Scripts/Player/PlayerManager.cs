using Cinemachine;
using FishNet.Connection;
using FishNet.Object;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;

public class PlayerManager : NetworkBehaviour
{
    [SerializeField] PlayerUnitSpawner playerUnitSpawner;
    [SerializeField] PlayerRaidSpawner playerRaidSpawner;
    [SerializeField] PlayerBaseSpawner playerBaseSpawner;
    [SerializeField] LayerMask unityLayerMast;
    [SerializeField] List<NetworkObject> infantryObjects;
    [SerializeField] List<NetworkObject> supportObjects;
    [SerializeField] List<NetworkObject> antiArmorObjects;
    [SerializeField] List<NetworkObject> armoredObjects;
    [SerializeField] List<NetworkObject> raidPrefabs;
    [SerializeField] TextMeshProUGUI goldAmount;
    [SerializeField] TextMeshProUGUI xpAmount;
    [SerializeField] string enemyTag;
    [SerializeField] int gold;
    [SerializeField] int xp;
    public MainCanvas MainCanvas;

    [HideInInspector] public List<Dictionary<HelperClass.UnitType, HelperClass.UnitProperties>> UnitsProperties;

    public CinemachineVirtualCamera MainVirtualCamera;
    public bool Inverted = false;

    Unit hoveredUnit;

    public override void OnStartClient()
    {
        base.OnStartClient();

        float angle = transform.eulerAngles.y > 180 ? transform.eulerAngles.y - 360 : transform.eulerAngles.y;
        Inverted = angle < 0;
        FillStrengthsDictionary();

        if (!base.IsOwner)
        {
            GetComponent<PlayerManager>().enabled = false;
            return;
        }

        goldAmount.text = gold.ToString();
        xpAmount.text = xp.ToString() + " XP";
    }

    void Update()
    {
        if (!base.IsOwner) return;
        OnMouseHover();
    }

    public Base GetCurrentBase()
    {
        return playerBaseSpawner.GetCurrentBase();
    }

    public int GetLevel()
    {
        return playerBaseSpawner.GetCurrentBase().Level;
    }

    void FillStrengthsDictionary()
    {
        // TODO: Update prices etc
        UnitsProperties = new()
        {
            new() {
                { HelperClass.UnitType.Infantry, new HelperClass.UnitProperties(25, 35, 140, 100, 100) },
                { HelperClass.UnitType.Support, new HelperClass.UnitProperties(15, 30, 100, 110, 100) },
                { HelperClass.UnitType.AntiArmor, new HelperClass.UnitProperties(30, 40, 175, 120, 100) },
                { HelperClass.UnitType.Armored, new HelperClass.UnitProperties(30, 50, 350, 130, 100) }
            },
            new() {
                { HelperClass.UnitType.Infantry, new HelperClass.UnitProperties(55, 65, 140, 200, 100) },
                { HelperClass.UnitType.Support, new HelperClass.UnitProperties(25, 40, 100, 210, 100) },
                { HelperClass.UnitType.AntiArmor, new HelperClass.UnitProperties(40, 50, 175, 220, 100) },
                { HelperClass.UnitType.Armored, new HelperClass.UnitProperties(300, 500, 350, 230, 100) }
            },
            new() {
                { HelperClass.UnitType.Infantry, new HelperClass.UnitProperties(55, 65, 140, 300, 100) },
                { HelperClass.UnitType.Support, new HelperClass.UnitProperties(25, 40, 100, 310, 100) },
                { HelperClass.UnitType.AntiArmor, new HelperClass.UnitProperties(40, 50, 175, 320, 100) },
                { HelperClass.UnitType.Armored, new HelperClass.UnitProperties(300, 500, 350, 330, 100) }
            },
            new() {
                { HelperClass.UnitType.Infantry, new HelperClass.UnitProperties(55, 65, 140, 400, 100) },
                { HelperClass.UnitType.Support, new HelperClass.UnitProperties(25, 40, 100, 410, 100) },
                { HelperClass.UnitType.AntiArmor, new HelperClass.UnitProperties(40, 50, 175, 420, 100) },
                { HelperClass.UnitType.Armored, new HelperClass.UnitProperties(300, 500, 350, 430, 100) }
            },
        };
    }

    [ObserversRpc]
    public void AddGold(int targetClientId, int amount)
    {
        if (LocalConnection.ClientId != targetClientId) return;

        gold += amount;
        UpdateGoldAndXPText();
    }

    public void RemoveGold(int amount)
    {
        gold -= amount;
        UpdateGoldAndXPText();
    }

    public int GetGold() { return gold; }
    public int GetXP() { return xp; }

    List<Unit> GetAllUnits()
    {
        return GameObject
            .FindGameObjectsWithTag("Enemy") // TODO: tag from helper class
            .Select(u => u.GetComponent<Unit>())
            .Where(u => u.inverted.Value == Inverted)
            .ToList();
    }

    public void UpgradeUnitHealth(int unitType)
    {
        var units = GetAllUnits()
            .Where(u => (int)u.unitType.Value == unitType);

        float hpMultiplier = 1.1f;

        foreach(var u in units)
            u.hp.Value = (int)(hpMultiplier * u.hp.Value);

        for(int level = 0; level < 4; level++)
        {
            var unitProperties = UnitsProperties[level][(HelperClass.UnitType)unitType];
            int hp = (int)(hpMultiplier * unitProperties.hp);

            UnitsProperties[level][(HelperClass.UnitType)unitType] =
                new HelperClass.UnitProperties(unitProperties.damageMin, unitProperties.damageMax, hp, unitProperties.price, unitProperties.movementSpeed);
        }
    }

    public void UpgradeUnitDamage(int unitType)
    {
        var units = GetAllUnits()
            .Where(u => (int)u.unitType.Value == unitType);

        float damageMultiplier = 1.1f;

        foreach (var u in units)
        {
            u.minDamage.Value = (int)(damageMultiplier * u.minDamage.Value);
            u.maxDamage.Value = (int)(damageMultiplier * u.maxDamage.Value);
        }

        for (int level = 0; level < 4; level++)
        {
            var unitProperties = UnitsProperties[level][(HelperClass.UnitType)unitType];
            int damageMin = (int)(damageMultiplier * unitProperties.damageMin);
            int damageMax = (int)(damageMultiplier * unitProperties.damageMax);

            UnitsProperties[level][(HelperClass.UnitType)unitType] =
                new HelperClass.UnitProperties(damageMin, damageMax, unitProperties.hp, unitProperties.price, unitProperties.movementSpeed);
        }
    }

    public void UpgradeUnitMovementSpeed()
    {
        var units = GetAllUnits();

        int speedAdder = 25;

        foreach (var u in units)
        {
            // TODO: syncvar not working for client, [ServerRpc] private void SetName(string value) => Name.Value = value;
            u.moveSpeed.Value = speedAdder + u.moveSpeed.Value;
            u.UpdateAnimationSpeed();
        }

        for (int level = 0; level < 4; level++)
        {
            var unitProperties = UnitsProperties[level][0];
            int movementSpeed = speedAdder + unitProperties.movementSpeed;

            for (int unitType = 0; unitType < 4; unitType++)
            {
                UnitsProperties[level][(HelperClass.UnitType)unitType] =
                    new HelperClass.UnitProperties(unitProperties.damageMin, unitProperties.damageMax, unitProperties.hp, unitProperties.price, movementSpeed);
            }
        }
    }

    public void UpgradeBase()
    {
        MainCanvas.Upgrade(GetLevel() + 1);
        playerBaseSpawner.UpgradeBase();
    }

    public void SpawnRaid()
    {
        if (gold - HelperClass.RaidPrice < 0)
            return;

        bool spawned = playerRaidSpawner.SpawnRaid(raidPrefabs[playerBaseSpawner.GetCurrentBase().Level]);

        if (!spawned) return;

        gold -= HelperClass.RaidPrice;
        UpdateGoldAndXPText();
    }

    public void SpawnUnit(int unitType)
    {
        var baseLevel = playerBaseSpawner.GetCurrentBase().Level;
        NetworkObject unitObject = null;

        switch ((HelperClass.UnitType)unitType)
        {
            case HelperClass.UnitType.Infantry:
                unitObject = infantryObjects[baseLevel];
                break;
            case HelperClass.UnitType.Support:
                unitObject = supportObjects[baseLevel];
                break;
            case HelperClass.UnitType.AntiArmor:
                unitObject = antiArmorObjects[baseLevel];
                break;
            case HelperClass.UnitType.Armored:
                unitObject = armoredObjects[baseLevel];
                break;
        }

        int price = unitObject.GetComponent<Unit>().GetPrice();

        if (gold - price < 0)
            return;

        bool spawned = false;
        if (unitObject != null)
            spawned = playerUnitSpawner.SpawnInfantry(unitObject);

        if (!spawned) return;

        RemoveGold(price);
    }

    void OnMouseHover()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (gameObject.scene.GetPhysicsScene().Raycast(ray.origin, ray.direction, out hit, Mathf.Infinity, unityLayerMast))
        {
            var hittedObject = hit.collider.gameObject;
            if (hittedObject.CompareTag(enemyTag))
            {
                if (hittedObject.GetComponent<Unit>() != hoveredUnit && hoveredUnit)
                    hoveredUnit.healthBar.gameObject.SetActive(false);
                hoveredUnit = hittedObject.GetComponent<Unit>();
                hoveredUnit.healthBar.gameObject.SetActive(true);
            }
        }
        else
        {
            if (!hoveredUnit) return;
            hoveredUnit.healthBar.gameObject.SetActive(false);
            hoveredUnit = null;
        }
    }

    void UpdateGoldAndXPText()
    {
        goldAmount.text = gold.ToString();
        xpAmount.text = xp.ToString() + " XP";
    }
}
