using FishNet.Managing.Server;
using FishNet.Object;
using UnitySceneManager = UnityEngine.SceneManagement.SceneManager;
using UnityEngine;

public class PlayerRaidSpawner : NetworkBehaviour
{
    [SerializeField] PlayerManager playerManager;
    [SerializeField] CircularSlider timer;

    public override void OnStartClient()
    {
        base.OnStartClient();
        if (!base.IsOwner)
            GetComponent<PlayerRaidSpawner>().enabled = false;
    }

    // true if can spawn
    public bool SpawnRaid(NetworkObject raid)
    {
        if (timer.Counting) return false;

        timer.StartCounting();

        SpawnObject(raid, transform, playerManager.Inverted);

        return true;
    }

    [ServerRpc]
    public void SpawnObject(NetworkObject obj, Transform player, bool inverted)
    {
        var raid = obj.gameObject.GetComponent<Raid>();
        raid.PlayerManager = playerManager;

        var initialPositionX = inverted ? raid.InitialPosition.x * -1 : raid.InitialPosition.x;

        NetworkObject nob = Instantiate<NetworkObject>(obj, 
            new Vector3(initialPositionX, raid.InitialPosition.y, raid.InitialPosition.z),
            player.transform.rotation);

        UnitySceneManager.MoveGameObjectToScene(nob.gameObject, gameObject.scene);

        ServerManager.Spawn(nob, base.Owner);

        SetSpawnedObject(nob);
    }

    [ObserversRpc]
    public void SetSpawnedObject(NetworkObject spawned)
    {
        UnitySceneManager.MoveGameObjectToScene(spawned.gameObject, gameObject.scene);
        spawned.gameObject.GetComponent<Raid>().PlayerManager = playerManager;
    }

    [ServerRpc(RequireOwnership = false)]
    public void DespawnObject(NetworkObject obj)
    {
        ServerManager.Despawn(obj);
    }
}
