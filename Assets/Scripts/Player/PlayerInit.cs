using FirstGearGames.LobbyAndWorld.Demos.KingOfTheHill;
using FishNet.Object;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PlayerInit : NetworkBehaviour
{
    [SerializeField] NetworkObject playerPrefab;

    bool spawned = false;

    public override void OnStartClient()
    {
        base.OnStartClient();
        if (!base.IsOwner)
        {
            GetComponent<PlayerInit>().enabled = false;
            return;
        }
    }

    void Update()
    {
        if (spawned) return;

        bool gameSceneLoaded = CheckScene();

        if (gameSceneLoaded)
        {
            Transform spawnPoint;
            if (base.IsHost)
                spawnPoint = GameObject.FindGameObjectWithTag("SpawnPointHost").transform;
            else
                spawnPoint = GameObject.FindGameObjectWithTag("SpawnPointClient").transform;
            spawned = true;
            SpawnPlayer(playerPrefab, spawnPoint.position, spawnPoint.rotation);
        }
    }

    bool CheckScene()
    {
        return gameObject.scene.name == "Game";
    }

    [ServerRpc]
    public void SpawnPlayer(NetworkObject obj, Vector3 position, Quaternion rotation)
    {
        NetworkObject nob = Instantiate<NetworkObject>(obj, position, rotation);
        ServerManager.Spawn(nob, base.Owner);
    }
}
