using UnityEngine;

public class SpareRaid : Raid
{
    [SerializeField] float fallSpeed;
    [SerializeField] GameObject virtualCamera;
    [SerializeField] float virtualCameraShowTime;
    [SerializeField] float despawnTime;

    bool inverted = false;

    public override void OnStartClient()
    {
        base.OnStartClient();

        if (base.IsServerInitialized)
            virtualCamera.SetActive(false);

        if (!base.IsOwner) return;
        
        float angle = transform.eulerAngles.y > 180 ? transform.eulerAngles.y - 360 : transform.eulerAngles.y;
        inverted = angle < 0;

        if(inverted)
        {
            var tempRotation = virtualCamera.transform.eulerAngles;
            tempRotation.y += 90f;
            virtualCamera.transform.eulerAngles = tempRotation;
            var tempPosition = virtualCamera.transform.localPosition;
            tempPosition.x *= -1;
            virtualCamera.transform.localPosition = tempPosition;
        }
    }

    public void AddGold(int gold)
    {
        PlayerManager.AddGold(base.OwnerId, gold);
    }

    void Update()
    {
        if (base.IsServerInitialized)
            Move();

       virtualCameraShowTime -= Time.deltaTime;

       if (virtualCameraShowTime <= 0)
           virtualCamera.SetActive(false);
    }

    void Move()
    {
        transform.Translate(new Vector3(0, -fallSpeed * Time.deltaTime, fallSpeed * Time.deltaTime));

        despawnTime -= Time.deltaTime;

        if (despawnTime <= 0)
            Despawn();
    }
}
