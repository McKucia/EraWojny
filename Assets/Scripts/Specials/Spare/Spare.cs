using FishNet.Object;
using UnityEngine;

public class Spare : NetworkBehaviour
{
    [SerializeField] string targetTag;
    [SerializeField] int damage;
    [SerializeField] SpareRaid spareRaid;
    [SerializeField] string groundTag;
    [SerializeField] ParticleSystem particleGroundHit;

    PlayerManager playerManager;

    public override void OnStartClient()
    {
        base.OnStartClient();
    }

    void Start()
    {
        if (!base.IsServerInitialized) return;

        playerManager = spareRaid.PlayerManager;
    }

    void Update()
    {
        if (!base.IsServerInitialized) return;
    }

    void OnTriggerEnter(Collider c)
    {
        if (c.gameObject.CompareTag(groundTag))
        {
            particleGroundHit.transform.parent = null;
            particleGroundHit.Play();
        }

        if (!base.IsServerInitialized) return;

        if (!c.gameObject.CompareTag(targetTag)) return;
        if (c.gameObject.GetComponent<NetworkObject>()?.OwnerId == this.OwnerId) return;

        TakeDamageToUnit(c.gameObject.GetComponent<Unit>());
    }

    void TakeDamageToUnit(Unit unit)
    {
        gameObject.SetActive(false);

        if (unit.hp.Value - damage <= 0)
            spareRaid.AddGold(100);

        unit.TakeDamage(damage, true);
        unit.DisplayDamage(damage);
        Despawn(gameObject);
    }
}
