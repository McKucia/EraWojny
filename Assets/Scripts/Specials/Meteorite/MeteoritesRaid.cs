using Cinemachine;
using FishNet.Object;
using System.Linq;
using UnityEngine;

public class MeteoritesRaid : Raid
{
    [SerializeField] float fallSpeed;
    [SerializeField] float virtualCameraNoiseTime;
    [SerializeField] float noiseAmplitude;
    [SerializeField] float noiseFrequency;
    [SerializeField] float despawnTime;

    void Start()
    {
        SetNoise(true);
    }

    void Update()
    {
        if (base.IsServerInitialized)
            Move();
    }

    public void AddGold(int gold)
    {
        PlayerManager.AddGold(base.OwnerId, gold);
    }

    void Move()
    {
        transform.Translate(Vector3.down * fallSpeed * Time.deltaTime);
        despawnTime -= Time.deltaTime;

        if (despawnTime <= 0)
            Despawn();
    }

    void SetNoise(bool active)
    {
        var vcamNoise = GameObject
            .FindGameObjectsWithTag(HelperClass.VirtualMainCameraTag)
            .Where(vcam => vcam.activeSelf == true)
            .Select(vcam => vcam.GetComponent<NoiseSetter>())
            .First();

        vcamNoise.SetNoise(virtualCameraNoiseTime, noiseAmplitude, noiseFrequency);
    }
}
