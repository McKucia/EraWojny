using FishNet.Object;
using UnityEngine;

public class Meteorite : NetworkBehaviour
{
    [SerializeField] string targetTag;
    [SerializeField] string groundTag;
    [SerializeField] int damage;
    [SerializeField] MeteoritesRaid meteoriteRaid;
    [SerializeField] ParticleSystem particleGroundHit;

    public override void OnStartClient()
    {
        base.OnStartClient();
    }

    void Update()
    {
        if (base.IsServerInitialized) return;
    }

    void OnTriggerEnter(Collider c)
    {
        if(c.gameObject.CompareTag(groundTag))
        {
            particleGroundHit.transform.parent = null;
            particleGroundHit.Play();
        }

        if (!base.IsServerInitialized) return;

        if (!c.gameObject.CompareTag(targetTag)) return;
        if (c.gameObject.GetComponent<NetworkObject>()?.OwnerId == this.OwnerId) return;

        TakeDamageToUnit(c.gameObject.GetComponent<Unit>());
    }

    void TakeDamageToUnit(Unit unit)
    {
        if (unit.hp.Value - damage <= 0)
            meteoriteRaid.AddGold(100);

        unit.TakeDamage(damage, true);
        unit.DisplayDamage(damage);
    }
}
