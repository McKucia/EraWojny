public class UpgradeMovementSpeed : Upgrade
{
    void Start()
    {
        Init();
        SetValues();
    }

    void SetValues()
    {
        int speedAdder = 25;
        int currentLevel = playerManager.GetLevel();

        int movementSpeed = playerManager.UnitsProperties[currentLevel][unitType].movementSpeed;

        value1Before.text = movementSpeed.ToString() + "%";
        value1After.text = (movementSpeed + speedAdder).ToString() + "%";
    }

    public void Upgrade(int unitType)
    {
        if (playerManager.GetGold() - price < 0)
            return;
        if (currentUpgradeLevel >= maxUpgradeLevel)
            return;
        playerManager.RemoveGold(price);
        playerManager.UpgradeUnitMovementSpeed();

        UpdateUI();
    }

    protected override void UpdateUI()
    {
        SetValues();
        base.UpdateUI();
    }
}
