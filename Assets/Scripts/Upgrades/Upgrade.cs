using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Upgrade : MonoBehaviour
{
    [SerializeField] protected PlayerManager playerManager;
    [SerializeField] protected HelperClass.UnitType unitType;

    [SerializeField] protected TextMeshProUGUI upgradePrice;
    [SerializeField] protected TextMeshProUGUI upgradeText;

    [SerializeField] protected TextMeshProUGUI value1Before;
    [SerializeField] protected TextMeshProUGUI value1After;
    [SerializeField] protected TextMeshProUGUI value2Before;
    [SerializeField] protected TextMeshProUGUI value2After;

    [SerializeField] protected GameObject upgradePriceContainer;
    [SerializeField] protected GameObject uprageValuesContainer;
    [SerializeField] protected GameObject[] grayStars;
    [SerializeField] protected GameObject[] goldStars;

    [SerializeField] protected int currentUpgradeLevel = 0;
    [SerializeField] protected int maxUpgradeLevel = 3;
    [SerializeField] protected int price;
    [SerializeField] protected List<int> pricesEachLevel;

    [SerializeField] protected Button upgradeButton;
    [SerializeField] protected Image upgradeImage;

    public void Init()
    {
        price = pricesEachLevel[currentUpgradeLevel];
        upgradePrice.text = price.ToString();
    }
    protected virtual void UpdateUI()
    {
        grayStars[currentUpgradeLevel].SetActive(false);
        goldStars[currentUpgradeLevel].SetActive(true);
        currentUpgradeLevel++;

        if (currentUpgradeLevel >= maxUpgradeLevel)
        {
            upgradePriceContainer.SetActive(false);
            uprageValuesContainer.SetActive(false);

            upgradeButton.interactable = false;
            var upgradeImageColor = upgradeImage.color;
            upgradeImageColor.a = 0.4f;

            upgradeImage.color = upgradeImageColor;
            upgradeText.color = upgradeImageColor;
        }
        else
        {
            price = pricesEachLevel[currentUpgradeLevel];
            upgradePrice.text = price.ToString();
        }
    }
}
