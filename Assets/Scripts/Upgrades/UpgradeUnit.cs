public class UpgradeUnit : Upgrade
{
    void Start()
    {
        Init();
        SetValues();    
    }

    void SetValues()
    {
        float multiplier = 1.1f;
        int currentLevel = playerManager.GetLevel();

        int hp = playerManager.UnitsProperties[currentLevel][unitType].hp;
        int minDamage = playerManager.UnitsProperties[currentLevel][unitType].damageMin;
        int maxDamage = playerManager.UnitsProperties[currentLevel][unitType].damageMax;

        value1Before.text = hp.ToString();
        value1After.text = ((int)(multiplier * hp)).ToString();

        value2Before.text = minDamage.ToString() + " - " + maxDamage.ToString();
        value2After.text = ((int)(multiplier * minDamage)).ToString() + " - " + ((int)(multiplier * maxDamage)).ToString();
    }

    public void Upgrade(int unitType)
    {
        if (playerManager.GetGold() - price < 0)
            return;
        if (currentUpgradeLevel >= maxUpgradeLevel)
            return;

        playerManager.RemoveGold(price);
        playerManager.UpgradeUnitHealth(unitType);
        playerManager.UpgradeUnitDamage(unitType);

        UpdateUI();
    }

    protected override void UpdateUI()
    {
        SetValues();
        base.UpdateUI();
    }
}
