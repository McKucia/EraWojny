﻿using FirstGearGames.LobbyAndWorld.Clients;
using FirstGearGames.LobbyAndWorld.Global;
using FirstGearGames.LobbyAndWorld.Global.Canvases;
using FirstGearGames.LobbyAndWorld.Lobbies;
using FirstGearGames.LobbyAndWorld.Lobbies.JoinCreateRoomCanvases;
using FishNet;
using FishNet.Connection;
using FishNet.Managing.Scened;
using FishNet.Object;
using FishNet.Object.Synchronizing;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnitySceneManager = UnityEngine.SceneManagement.SceneManager;

namespace FirstGearGames.LobbyAndWorld.Demos.KingOfTheHill
{


    public class GameplayManager : NetworkBehaviour
    {
        #region Serialized
        [Header("Spawning")]
        /// <summary>
        /// Region players may spawn.
        /// </summary>
        [Tooltip("Region players may spawn.")]
        [SerializeField]
        private List<Transform> _spawnPoints = new List<Transform>(2);
        /// <summary>
        /// Prefab to spawn.
        /// </summary>
        [Tooltip("Prefab to spawn.")]
        [SerializeField]
        private NetworkObject _playerPrefab = null;
        /// <summary>
        /// DeathDummy to spawn.
        /// </summary>
        [Tooltip("DeathDummy to spawn.")]
        [SerializeField]
        private GameObject _deathDummy = null;
        #endregion

        /// <summary>
        /// RoomDetails for this game. Only available on the server.
        /// </summary>
        private RoomDetails _roomDetails = null;
        /// <summary>
        /// LobbyNetwork.
        /// </summary>
        private LobbyNetwork _lobbyNetwork = null;
        /// <summary>
        /// Becomes true once someone has won.
        /// </summary>
        private bool _winner = false;
        /// <summary>
        /// Currently spawned player objects. Only exist on the server.
        /// </summary>
        private List<NetworkObject> _spawnedPlayerObjects = new List<NetworkObject>();

        int currentAge = 0;

        [SerializeField] string ageTerrainTag;

        [SerializeField] GameObject[] terrains;

        #region Initialization and Deinitialization.
        private void OnDestroy()
        {
            if (_lobbyNetwork != null)
            {
                _lobbyNetwork.OnClientJoinedRoom -= LobbyNetwork_OnClientStarted;
                _lobbyNetwork.OnClientLeftRoom -= LobbyNetwork_OnClientLeftRoom;
            }
        }

        public override void OnStartClient()
        {
            base.OnStartClient();

            terrains = GameObject.FindGameObjectsWithTag(ageTerrainTag);
            Array.Sort(terrains, delegate (GameObject x, GameObject y) { return x.name.CompareTo(y.name); });


            for (int i = 0; i < terrains.Length; i++)
                if (i != currentAge)
                    terrains[i].transform.Translate(new Vector3(0, 100, 0));
        }

        public void UpdateTerrain(int index)
        {
            if (index > currentAge)
            {
                terrains[index - 1].transform.Translate(new Vector3(0, 100, 0));
                terrains[index].transform.Translate(new Vector3(0, -100, 0));
                currentAge = index;
            }
        }

        /// <summary>
        /// Initializes this script for use.
        /// </summary>
        public void FirstInitialize(RoomDetails roomDetails, LobbyNetwork lobbyNetwork)
        {
            _roomDetails = roomDetails;
            _lobbyNetwork = lobbyNetwork;
            _lobbyNetwork.OnClientStarted += LobbyNetwork_OnClientStarted;
            _lobbyNetwork.OnClientLeftRoom += LobbyNetwork_OnClientLeftRoom;
        }

        /// <summary>
        /// Called when a client leaves the room.
        /// </summary>
        /// <param name="arg1"></param>
        /// <param name="arg2"></param>
        private void LobbyNetwork_OnClientLeftRoom(RoomDetails arg1, NetworkObject arg2)
        {
            //Destroy all of clients objects, except their client instance.
            for (int i = 0; i < _spawnedPlayerObjects.Count; i++)
            {
                NetworkObject entry = _spawnedPlayerObjects[i];
                //Entry is null. Remove and iterate next.
                if (entry == null)
                {
                    _spawnedPlayerObjects.RemoveAt(i);
                    i--;
                    continue;
                }

                //If same connection to client (owner) as client instance of leaving player.
                if (_spawnedPlayerObjects[i].Owner == arg2.Owner)
                {
                    //Destroy entry then remove from collection.
                    entry.Despawn();
                    _spawnedPlayerObjects.RemoveAt(i);
                    i--;                        
                }

            }
        }

        /// <summary>
        /// Called when a client starts a game.
        /// </summary>
        /// <param name="roomDetails"></param>
        /// <param name="client"></param>
        private void LobbyNetwork_OnClientStarted(RoomDetails roomDetails, NetworkObject client)
        {
             SpawnPlayers();
        }
        #endregion

        #region Death.
        /// <summary>
        /// Called when object exits trigger. Used to respawn players.
        /// </summary>
        /// <param name="other"></param>
        private void OnTriggerExit(Collider other)
        {
            if (!base.IsServer)
                return;

            NetworkObject netIdent = other.gameObject.GetComponent<NetworkObject>();
            //If doesn't have a netIdent or no owning client exit.
            if (netIdent == null || netIdent.Owner == null)
                return;

            //If there is an owning client then destroy the object and respawn.
            StartCoroutine((__DelayRespawn(netIdent)));
        }

        /// <summary>
        /// Destroys netIdent and respawns player after delay.
        /// </summary>
        /// <param name="netIdent"></param>
        /// <returns></returns>
        private IEnumerator __DelayRespawn(NetworkObject netIdent)
        {
            //Send Rpc to spawn death dummy then destroy original.
            RpcSpawnDeathDummy(netIdent.transform.position);
            NetworkConnection conn = netIdent.Owner;
            InstanceFinder.ServerManager.Despawn(netIdent.gameObject);

            //Wait a little to respawn player.
            yield return new WaitForSeconds(1f);
            //Don't respawn if someone won.
            if (_winner)
                yield break;
            /* Check for rage quit conditions (left room). */
            if (conn == null)
                yield break;
            ClientInstance ci = ClientInstance.ReturnClientInstance(conn);
            if (ci == null || !_roomDetails.StartedMembers.Contains(ci.NetworkObject))
                yield break;

            SpawnPlayer(conn);
        }

        /// <summary>
        /// Spawns a dummy player to show death.
        /// </summary>
        /// <param name="player"></param>
        [ObserversRpc]
        private void RpcSpawnDeathDummy(Vector3 position)
        {
            GameObject go = Instantiate(_deathDummy, position, Quaternion.identity);
            UnitySceneManager.MoveGameObjectToScene(go, gameObject.scene);
            Destroy(go, 1f);
        }
        #endregion

        #region Winning.
        // mcq
        public void PlayerWon(NetworkObject winner)
        {
            if (_winner)
                return;
            _winner = true;

            StartCoroutine(__PlayerWon(winner));
        }

        /// <summary>
        /// Ends the game announcing winner and sending clients back to lobby.
        /// </summary>
        /// <returns></returns>
        private IEnumerator __PlayerWon(NetworkObject winner)
        {
            

            //Send out winner text.
            ClientInstance ci = ClientInstance.ReturnClientInstance(winner.Owner);
            string playerName = ci.PlayerSettings.GetUsername();
            foreach (NetworkObject item in _roomDetails.StartedMembers)
            {
                if (item != null && item.Owner != null)
                    TargetShowWinner(item.Owner, playerName, (item.Owner == winner.Owner));
            }

            //Wait a moment then kick the players out. Not required.
            yield return new WaitForSeconds(4f);
            List<NetworkObject> collectedIdents = new List<NetworkObject>();
            foreach (NetworkObject item in _roomDetails.StartedMembers)
            {
                ClientInstance cli = ClientInstance.ReturnClientInstance(item.Owner);
                if (ci != null)
                    collectedIdents.Add(cli.NetworkObject);
            }
            foreach (NetworkObject item in collectedIdents)
                _lobbyNetwork.TryLeaveRoom(item);
        }

        /// <summary>
        /// Displayers who won.
        /// </summary>
        /// <param name="winner"></param>
        [TargetRpc]
        private void TargetShowWinner(NetworkConnection conn, string winnerName, bool won)
        {
            string text = (won) ? "Victory!" :
                "DEFEAT";
            GlobalManager.CanvasesManager.EndCanvas.Show(text);
        }
        #endregion

        #region Spawning.
        private void SpawnPlayers()
        {
            foreach (var client in _roomDetails.StartedMembers)
                SpawnPlayer(client.Owner);
        }
        /// <summary>
        /// Spawns a player at a random position for a connection.
        /// </summary>
        /// <param name="conn"></param>
        private void SpawnPlayer(NetworkConnection conn)
        {
            Debug.Log("spawn player");
            var spawnPoint = _spawnPoints.First();
            _spawnPoints.Remove(spawnPoint);

            //Make object and move it to proper scene.
            NetworkObject nob = Instantiate<NetworkObject>(_playerPrefab, spawnPoint.position, spawnPoint.rotation);
            UnitySceneManager.MoveGameObjectToScene(nob.gameObject, gameObject.scene);

            _spawnedPlayerObjects.Add(nob);
            base.Spawn(nob.gameObject, conn);
           
            nob.transform.position = spawnPoint.position;
            ObserversTeleport(nob, spawnPoint.position);
        }
        /// <summary>
        /// teleports a NetworkObject to a position.
        /// </summary>
        /// <param name="ident"></param>
        /// <param name="position"></param>
        [ObserversRpc]
        private void ObserversTeleport(NetworkObject ident, Vector3 position)
        {
            UnitySceneManager.MoveGameObjectToScene(ident.gameObject, gameObject.scene);
            ident.transform.position = position;
        }
        #endregion
    }


}