using FishNet.CodeGenerating;
using FishNet.Component.Animating;
using FishNet.Object;
using FishNet.Object.Synchronizing;
using System.Collections;
using UnityEngine;

public class Unit : Attackable
{
    [SerializeField] protected float spawnYPosition;
    [SerializeField] protected float attackDistance;
    [SerializeField] protected float fadeSpeed;
    [SerializeField] protected string enemyTag; // TODO move to helper class
    [SerializeField] protected string baseTag;
    [SerializeField] protected int numAttacks = 4;
    [SerializeField] protected int price;
    [SerializeField] protected Transform bloodRoot;
    [SerializeField] protected Animator animator;
    [SerializeField] protected NetworkAnimator networkAnimator;
    [SerializeField] protected ParticleSystem particleBlood;
    [SerializeField] protected ParticleSystem particleGuts;
    [SerializeField] protected Renderer objRenderer;
    [SerializeField] protected Collider objCollider;

    public Transform turretTarget;

    [AllowMutableSyncType]
    public SyncVar<int> moveSpeed = new();

    [AllowMutableSyncType]
    public SyncVar<int> maxDamage = new();

    [AllowMutableSyncType]
    public SyncVar<int> minDamage = new();

    readonly protected SyncVar<bool> isMoving = new(true);
    readonly protected SyncVar<bool> isAttacking = new(false);

    [HideInInspector] public readonly SyncVar<bool> isDying = new(false);
    readonly protected SyncVar<Attackable> currentAttackedObject = new();

    [AllowMutableSyncType]
    public SyncVar<HelperClass.UnitType> unitType = new();

    protected int prevAttackType = 0;

    public override void OnStartServer()
    {
        base.OnStartServer();
        animator.SetBool("IsWalking", true);
    }

    protected override void Init()
    {
        base.Init();

        attackableType.Value = HelperClass.AttackableType.Unit;

        isMoving.OnChange += OnIsMoving;
        isAttacking.OnChange += OnIsAttacking;
    }

    protected void Update()
    {
        if (IsServerInitialized)
            MoveServer();
    }

    void MoveServer()
    {
        if (isMoving.Value && !isDying.Value)
            transform.position += new Vector3(inverted.Value ? -1 : 1, 0f, 0f) * moveSpeed.Value / 50f * Time.deltaTime;
    }

    void OnIsMoving(bool prev, bool next, bool asServer)
    {
        if (!asServer) return;
        if (next == false)
            animator.SetBool("IsWalking", false);
        else
            animator.SetBool("IsWalking", true);
    }

    void OnIsAttacking(bool prev, bool next, bool asServer)
    {
        if (next == true)
            Attack();
    }

    protected override void OnHP(int prev, int next, bool asServer)
    {
        base.OnHP(prev, next, asServer);

        if (hp.Value < InitialHP / 3)
        {
            particleBlood.transform.position = bloodRoot.position;
            particleBlood.Play();
        }
    }

    public void UpdateAnimationSpeed()
    {
        animator.SetFloat("MovementSpeedMultiplier", moveSpeed.Value / 100f);
    }

    public void SetProperties(int initMinDamage, int initMaxDamage, int initHP, int movementSpeed)
    {
        minDamage.Value = initMinDamage;
        maxDamage.Value = initMaxDamage;
        hp.Value = initHP;
        moveSpeed.Value = movementSpeed;
        UpdateAnimationSpeed();
    }

    public int GetPrice()
    {
        return price;
    }

    public override void TakeDamage(int damage, bool fromTurret = false)
    {
        if (isDying.Value) return;

        base.TakeDamage(damage);

        if (fromTurret)
            PlayGuts();

        if (hp.Value <= 0)
        {
            animator.SetBool("IsDying", true);
            isMoving.Value = false;
            isAttacking.Value = false;
            isDying.Value = true;
            objCollider.enabled = false;
            RemoveCollider();
            StartCoroutine(DespawnUnit());
        }
    }

    [ObserversRpc]
    void RemoveCollider()
    {
        objCollider.enabled = false;
    }

    [ObserversRpc]
    void PlayGuts()
    {
        particleGuts.transform.position = bloodRoot.position;
        particleGuts.Play();
    }

    IEnumerator DespawnUnit()
    {
        yield return new WaitForSeconds(3f);
        Despawn(gameObject);
    }

    #region Animation Events

    public virtual void Attack()
    {
    }

    public virtual void Hit()
    {
        if (!base.IsServer) return;

        var damage = Random.Range(minDamage.Value, maxDamage.Value);

        if (currentAttackedObject.Value.hp.Value - damage <= 0)
        {
            if (currentAttackedObject.Value.attackableType.Value == HelperClass.AttackableType.Unit)
            {
                isMoving.Value = true;
                isAttacking.Value = false;
                PlayerManager.AddGold(base.OwnerId, 100); // TODO
            }
            else if (currentAttackedObject.Value.attackableType.Value == HelperClass.AttackableType.Base)
            {
                // gameplayManager.PlayerWon(GetComponent<NetworkObject>());
                isMoving.Value = false;
                isAttacking.Value = false;
            }
        }

        currentAttackedObject.Value.TakeDamage(damage);
        currentAttackedObject.Value.DisplayDamage(damage);
    }

    #endregion
}
