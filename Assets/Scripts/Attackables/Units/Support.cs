using FishNet.Object;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnitySceneManager = UnityEngine.SceneManagement.SceneManager;

public class Support : Unit
{
    [SerializeField] string throwDistanceCheckerName;
    [SerializeField] string attackDistanceCheckerName;
    [SerializeField] GameObject bulletRoot;
    [SerializeField] NetworkObject bottlePrefab;
    [SerializeField] GameObject bulletObject;
    [SerializeField] ParticleSystem particleShoot;
    [SerializeField] Transform pyroTarget;

    List<Unit> unitsInThrowRange = new();
    bool isThrowing = false;

    Stone stone;
    Bottle bottle;

    void Start()
    {
        base.Init();
    }

    void Update()
    {
        base.Update();

        if (IsServerInitialized) { 
            CheckCollideDistanceServer(transform.position, transform.forward);
            CheckThrowPossibility();
        }
    }

    void CheckCollideDistanceServer(Vector3 position, Vector3 direction)
    {
        if (isAttacking.Value) return;

        RaycastHit hit;
        if (gameObject.scene.GetPhysicsScene().Raycast(position, direction, out hit, attackDistance))
        {
            var hittedObject = hit.collider.gameObject;
            if (hittedObject == this.gameObject) return;

            if (hittedObject.CompareTag(enemyTag))
            {
                var unit = hittedObject.GetComponent<Unit>();
                if (!unit) return;

                if (unit.inverted.Value == this.inverted.Value)
                {
                    isMoving.Value = false;
                }
                else
                {
                    SetThrowing(false);
                    isMoving.Value = false;
                    isAttacking.Value = true;
                    currentAttackedObject.Value = unit;
                }
            }

            if (hittedObject.CompareTag(baseTag))
            {
                var basee = hittedObject.GetComponent<Base>();
                if (!basee) return;

                isMoving.Value = false;
                isAttacking.Value = true;
                currentAttackedObject.Value = basee;
            }
        }
        else
        {
            isMoving.Value = true;
        }
    }

    // called from distance checker
    public void EnemyEnterCallback(string distanceCheckerName, Unit unit)
    {
        if (!IsServerInitialized) return;

        if (distanceCheckerName == throwDistanceCheckerName) // rival
        {
            unitsInThrowRange.Add(unit);
        }
    }

    public void EnemyExitCallback(string distanceCheckerName, Unit unit)
    {
        if (!IsServerInitialized) return;

        if (distanceCheckerName == throwDistanceCheckerName)
        {
            unitsInThrowRange.Remove(unit);
        }
    }

    void SetThrowing(bool active)
    {
        if (isThrowing == active) return;

        isThrowing = active;
        animator.SetBool("IsThrowing", active);
        StartCoroutine(SetLayerWeightSmooth(active));
    }

    void CheckThrowPossibility()
    {
        if (isAttacking.Value == true || currentAttackedObject.Value != null) return;

        currentAttackedObject.Value = unitsInThrowRange.FirstOrDefault(u => u.isDying.Value == false);

        if (currentAttackedObject.Value == null)
            SetThrowing(false);
        else
            SetThrowing(true);
    }

    public override void Hit()
    {
        base.Hit();

        if(isThrowing)
            currentAttackedObject.Value = null;
    }

    IEnumerator SetLayerWeightSmooth(bool active)
    {
        float currentWeight;
        float duration = 0.5f;
        float normalizedTime = 0;

        while (normalizedTime <= 1f)
        {
            currentWeight = active ? normalizedTime : 1f - normalizedTime;
            normalizedTime += Time.deltaTime / duration;
            animator.SetLayerWeight(1, currentWeight);
            yield return null;
        }
        currentWeight = active ? 1f : 0f;
        animator.SetLayerWeight(1, currentWeight);
    }

    public void PlayShootParticle()
    {
        particleShoot.Play();
    }

    public void PlayFireParticle()
    {
        if (base.IsServerInitialized) return;

        if (bottle != null)
            bottle.PlayFireParticle();
    }

    public void ThrowBottle()
    {
        bottle.transform.parent = null;
        bottle.Shoot();
    }

    public void SpawnBottle()
    {
        if (base.IsServerInitialized)
        {
            NetworkObject nob = Instantiate<NetworkObject>(bottlePrefab, bulletRoot.transform);

            bottle = nob.GetComponent<Bottle>();

            bottle.Init(pyroTarget, PlayerManager);

            ServerManager.Spawn(nob, base.Owner);

            SetBottle(nob);
        }
        else
        {
            GameObject nob = Instantiate(bottlePrefab.gameObject, bulletRoot.transform);
        
            bottle = nob.GetComponent<Bottle>();
        
            bottle.Init(pyroTarget, PlayerManager);
        }
    }

    [ObserversRpc]
    public void SetBottle(NetworkObject spawned)
    {
        spawned.gameObject.SetActive(false);
    }

    public void Throw()
    {
        stone.transform.parent = null;
        stone.Shoot();
    }

    public void SpawnBullet()
    {
        var stoneObject = Instantiate(bulletObject, bulletRoot.transform);
        stone = stoneObject.GetComponent<Stone>();
        stone.Init(inverted.Value, currentAttackedObject.Value.ThrowTarget);
    }

    [ObserversRpc]
    public void SetSpawnedObject(NetworkObject spawned)
    {
        UnitySceneManager.MoveGameObjectToScene(spawned.gameObject, gameObject.scene);
    }

    public override void Attack()
    {
        if (!isAttacking.Value || !base.IsServerInitialized)
            return;

        base.Attack();

        int attackType = prevAttackType;

        if (numAttacks > 1)
        {
            while (attackType == prevAttackType)
                attackType = Random.Range(0, numAttacks);
        }

        prevAttackType = attackType;

        animator.SetInteger("AttackType", attackType);
        networkAnimator.SetTrigger("Attack");
    }
}
