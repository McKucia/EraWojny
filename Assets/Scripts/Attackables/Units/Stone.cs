using FishNet.Object;
using UnityEngine;

public class Stone : MonoBehaviour
{
    [SerializeField] int damage;
    [SerializeField] float speed;
    [SerializeField] string enemyTag;
    [SerializeField] float despawnTime;

    PlayerManager playerManager;
    bool fired = false;
    bool inverted;
    Transform targetRoot;


    public void Init(bool inv, Transform targetR)
    {
        inverted = inv;
        targetRoot = targetR;
    }

    public void Shoot()
    {
        fired = true;
    }

    void Update()
    {
       if (fired)
           Move();

        CheckDespawn();
    }

    void CheckDespawn()
    {
        despawnTime -= Time.deltaTime;

        if (despawnTime <= 0)
            Destroy(gameObject);
    }

    void Move()
    {
        if (targetRoot == null)
        {
            Destroy(gameObject);
            return;
        }

        transform.position = Vector3.MoveTowards(transform.position, targetRoot.position, Time.deltaTime * speed);
        transform.position -= new Vector3(0f, Time.deltaTime * 2f, 0f);
    }

    void OnTriggerEnter(Collider c)
    {
        if (!c.gameObject.CompareTag(enemyTag)) return;
        if (c.gameObject.GetComponent<Unit>().inverted.Value == inverted) return; // TODO not working
        // Debug.Log(c.gameObject.name);

        Destroy(gameObject);
    }
}
