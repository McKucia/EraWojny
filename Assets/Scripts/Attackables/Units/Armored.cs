using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Armored : Unit
{
    void Start()
    {
        base.Init();
    }

    void Update()
    {
        base.Update();

        if (IsServerInitialized)
            CheckCollideDistanceServer(transform.position, transform.forward);
    }

    void CheckCollideDistanceServer(Vector3 position, Vector3 direction)
    {
        if (isAttacking.Value) return;

        RaycastHit hit;
        if (gameObject.scene.GetPhysicsScene().Raycast(position, direction, out hit, attackDistance))
        {
            var hittedObject = hit.collider.gameObject;
            if (hittedObject == this.gameObject) return;

            if (hittedObject.CompareTag(enemyTag))
            {
                var unit = hittedObject.GetComponent<Unit>();
                if (!unit) return;

                if (unit.inverted.Value == this.inverted.Value)
                {
                    isMoving.Value = false;
                }
                else
                {
                    isMoving.Value = false;
                    isAttacking.Value = true;
                    currentAttackedObject.Value = unit;
                }
            }

            if (hittedObject.CompareTag(baseTag))
            {
                var basee = hittedObject.GetComponent<Base>();
                if (!basee) return;

                isMoving.Value = false;
                isAttacking.Value = true;
                currentAttackedObject.Value = basee;
            }
        }
        else
        {
            isMoving.Value = true;
        }
    }

    public override void Attack()
    {
        if (!isAttacking.Value || !base.IsServerInitialized)
            return;

        base.Attack();

        int attackType = prevAttackType;

        if (numAttacks > 1)
        {
            while (attackType == prevAttackType)
                attackType = Random.Range(0, numAttacks);
        }

        prevAttackType = attackType;

        animator.SetInteger("AttackType", attackType);
        networkAnimator.SetTrigger("Attack");
    }

    public override void Hit()
    {
        base.Hit();
    }
}
