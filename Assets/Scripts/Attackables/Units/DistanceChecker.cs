using FishNet.Object;
using System.Collections;
using System.Collections.Generic;
using Unity.Burst.CompilerServices;
using UnityEngine;

public class DistanceChecker : NetworkBehaviour
{
    [SerializeField] Support supportUnit;
    [SerializeField] string enemyTag; // TODO move to helper class

    void OnTriggerEnter(Collider other)
    {
        if (!IsServerInitialized) return;

        var hittedObject = other.gameObject;

        if (!hittedObject.CompareTag(enemyTag)
            || hittedObject == this.gameObject
            || hittedObject.GetComponent<NetworkObject>()?.Owner == this.Owner) return;

        var unit = hittedObject.GetComponent<Unit>();
        if (!unit) return;

        supportUnit.EnemyEnterCallback(this.name, unit);
    }

    void OnTriggerExit(Collider other)
    {
        if (!IsServerInitialized) return;

        var hittedObject = other.gameObject;

        if (!hittedObject.CompareTag(enemyTag)
            || hittedObject == this.gameObject 
            || hittedObject.GetComponent<NetworkObject>()?.Owner == this.Owner) return;

        var unit = hittedObject.GetComponent<Unit>();
        if (!unit) return;

        supportUnit.EnemyExitCallback(this.name, unit);
    }
}
