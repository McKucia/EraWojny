using FishNet.Object;
using System.Collections;
using UnityEngine;
using UnitySceneManager = UnityEngine.SceneManagement.SceneManager;

public class Bottle : NetworkBehaviour
{
    [SerializeField] int damage;
    [SerializeField] float speed;
    [SerializeField] string groundTag;
    [SerializeField] float despawnTime;
    [SerializeField] float range;
    [SerializeField] ParticleSystem fireParticle;
    [SerializeField] ParticleSystem groundFireParticle;
    [SerializeField] LayerMask enemyLayerMask;

    PlayerManager playerManager;
    Transform target;
    bool fired = false;

    public void Init(Transform tar, PlayerManager manager)
    {
        playerManager = manager;
        target = tar;
    }

    public void Shoot()
    {
        fired = true;
    }

    void Update()
    {
       if (fired)
           Move();

        CheckDespawn();
    }

    public void PlayFireParticle()
    {
        fireParticle.Play();
    }

    void CheckDespawn()
    {
        despawnTime -= Time.deltaTime;

        if (despawnTime <= 0)
            Destroy(gameObject);
    }

    void Move()
    {
        transform.position = Vector3.MoveTowards(transform.position, target.position, Time.deltaTime * speed);
        transform.position -= new Vector3(0f, Time.deltaTime * 2f, 0f);
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(transform.position, range);
    }


    void OnTriggerEnter(Collider c)
    {
        if (!c.gameObject.CompareTag(groundTag)) return;

        if (base.IsServerInitialized)
        {
            Collider[] hitColliders = new Collider[10];
            int numColliders =
                gameObject.scene.GetPhysicsScene().OverlapSphere(transform.position, range, hitColliders, enemyLayerMask.value, QueryTriggerInteraction.UseGlobal);
            for (int i = 0; i < numColliders; i++)
            {
                Collider hitCollider = hitColliders[i];
                if (hitCollider.gameObject.GetComponent<NetworkObject>()?.OwnerId == this.OwnerId) continue;
                TakeDamageToUnit(hitCollider.gameObject.GetComponent<Unit>());
                Debug.Log(hitCollider.gameObject.name);
            }
            Destroy(gameObject);
        }
        else
        {
            groundFireParticle.transform.SetParent(null);
            groundFireParticle.transform.rotation = Quaternion.identity;
            groundFireParticle.transform.Rotate(-90f, 0f, 0f);
            groundFireParticle.Play();

            Destroy(gameObject, 2f);
        }
    }

    void TakeDamageToUnit(Unit unit)
    {
        if (unit.hp.Value - damage <= 0)
            playerManager.AddGold(base.OwnerId, 100);
    
        unit.TakeDamage(damage, true);
        unit.DisplayDamage(damage);
    }
}
