using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class DamageUI : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI textMeshPro;
    [SerializeField] float fadeOutSpeed;

    public void Init(string text, bool rotate)
    {
        textMeshPro.text = text;
        if (rotate) transform.Rotate(0, 180, 0);
        StartCoroutine(FadeText(fadeOutSpeed));
    }

    IEnumerator FadeText(float duration)
    {
        float originalAlpha = textMeshPro.color.a;
        float elapsed = 0f;

        while (elapsed < duration)
        {
            transform.Translate(new Vector3(0, Time.deltaTime * 0.2f, 0));

            elapsed += Time.deltaTime;
            float alpha = Mathf.Lerp(originalAlpha, 0, elapsed / duration);
            textMeshPro.color = new Color(textMeshPro.color.r, textMeshPro.color.g, textMeshPro.color.b, alpha);
            yield return null;
        }

        Destroy(gameObject);
    }
}
