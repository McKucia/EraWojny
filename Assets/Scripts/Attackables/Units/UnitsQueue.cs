using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UnitsQueue : MonoBehaviour
{
    public List<Toggle> Toggles;

    public void UpdateQueue(int size)
    {
        int counter = 0;
        for(int i = 0; i < Toggles.Count; i++)
        {
            if (counter++ < size)
                Toggles[i].isOn = true;
            else
                Toggles[i].isOn = false;
        }
    }
}
