using System;
using UnityEngine;

public class EnemyAnimationEvents : MonoBehaviour
{
    [SerializeField] Unit unit;

    // animation event
    public void AttackEnd()
    {
        unit.Attack();
    }

    // animation event
    public void TakeDamage()
    {
        unit.Hit();
    }

    // shaman
    public void Heal()
    {
        var antiArmor = unit as AntiArmor;
        antiArmor.Heal();
    }

    // shaman
    public void PlayParticle(int index)
    {
        var antiArmor = unit as AntiArmor;
        antiArmor.PlayParticle(index);
    }

    // gunman
    public void PlayShootParticle()
    {
        var support = unit as Support;
        support.PlayShootParticle();
    }

    // pyro
    public void PlayFireParticle()
    {
        var support = unit as Support;
        support.PlayFireParticle();
    }

    // pyro
    public void SpawnBottle()
    {
        var support = unit as Support;
        support.SpawnBottle();
    }

    // pyro
    public void ThrowBottle()
    {
        var support = unit as Support;
        support.ThrowBottle();
    }

    // cave woman
    public void SpawnBullet()
    {
        var support = unit as Support;
        support.SpawnBullet();
    }

    // cave woman
    public void ThrowObject()
    {
        var support = unit as Support;
        support.Throw();
    }
}
