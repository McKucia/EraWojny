using FishNet.CodeGenerating;
using FishNet.Object;
using FishNet.Object.Synchronizing;
using UnityEngine;

public class Attackable : NetworkBehaviour
{
    public Camera PlayerCamera;
    public PlayerManager PlayerManager;
    public Transform ThrowTarget;

    [AllowMutableSyncType]
    [SerializeField]
    public SyncVar<int> hp = new();

    public int InitialHP;

    [AllowMutableSyncType]
    public SyncVar<HelperClass.AttackableType> attackableType = new();

    public HealthBar healthBar;

    [HideInInspector]
    [AllowMutableSyncType]
    public SyncVar<bool> inverted = new (false);

    const string gameplayManagerName = "GameplayManager";

    [SerializeField] GameObject damageUIPrefab;
    [SerializeField] Transform damageUIRoot;

    public override void OnStartClient()
    {
        base.OnStartClient();
    }

    void Start()
    {
        Init();
    }

    protected virtual void OnHP(int prev, int next, bool asServer)
    {
        if(healthBar)
            healthBar.UpdateHP(hp.Value);
    }

    protected virtual void Init()
    {
        InitialHP = hp.Value;

        hp.OnChange += OnHP;

        float angle = transform.eulerAngles.y > 180 ? transform.eulerAngles.y - 360 : transform.eulerAngles.y;
        inverted.Value = angle < 0;

        if(healthBar)
            healthBar.Init(PlayerCamera, inverted.Value, hp.Value);
    }

    public virtual void TakeDamage(int damage, bool fromTurret = false)
    {
        hp.Value -= damage;
    }

    [ObserversRpc]
    public void DisplayDamage(int damage)
    {
        var damageUI = Instantiate(damageUIPrefab, damageUIRoot);
        damageUI.GetComponent<DamageUI>().Init(damage.ToString(), inverted.Value);
    }
}
