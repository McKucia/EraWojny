using FishNet.Object;
using System;
using System.Collections.Generic;
using UnityEngine;

public class Base : Attackable
{
    [SerializeField] List<Transform> turretTowerSlots;
    [SerializeField] List<Drop> turretDrops;
    [SerializeField] GameObject turretsSlots;
    [SerializeField] int turretPrice = 200;
    [SerializeField] CheckBoxOverlap checkUnitInside;
    [SerializeField] GameObject baseVirtualCamera;

    MainCanvas mainCanvas;
    public int Level;

    public override void OnStartClient()
    {
        base.OnStartClient();

        mainCanvas = GameObject.FindGameObjectWithTag("MainCanvas").GetComponent<MainCanvas>();

        if (base.IsOwner)
            mainCanvas.InitHealthSlider(hp.Value, true);
    }

    public bool UnitInside()
    {
        return checkUnitInside.HowManyColliders() > 0;

    }
     
    protected override void Init()
    {
        base.Init();

        if (!base.IsOwner) return;
    }

    public void SetTurretsSlotsActive(bool active)
    {
        foreach (var turretDrop in turretDrops)
            turretDrop.CheckSpawn();
        mainCanvas.CameraMovement.CanMove = !active;
        baseVirtualCamera.SetActive(active);
        turretsSlots.SetActive(active);
    }

    protected override void OnHP(int prev, int next, bool asServer)
    {
        if(base.IsOwner)
            mainCanvas.UpdateHealthSlider(next, true);
        else
            mainCanvas.UpdateHealthSlider(next, false);
    }

    public override void TakeDamage(int damage, bool fromTurret = false)
    {
        base.TakeDamage(damage);

        if (hp.Value <= 0)
            Despawn(gameObject);
    }

    public void SpawnTurret(NetworkObject turretPrefab, int index)
    {
        if (!base.IsOwner) return;

        if (PlayerManager.GetGold() - turretPrice < 0) // TODO: add not enough money info popup or smth
            return;

        SpawnObject(turretPrefab, turretTowerSlots[index]);

        PlayerManager.RemoveGold(turretPrice);
    }

    [ServerRpc]
    public void SpawnObject(NetworkObject obj, Transform parent)
    {
        NetworkObject nob = Instantiate<NetworkObject>(obj, parent.position, parent.rotation, parent);
        nob.GetComponent<Turret>().PlayerManager = PlayerManager;
        ServerManager.Spawn(nob, base.Owner);
        UpdateTurretPosition(nob, nob.transform.position, nob.transform.rotation);
    }

    [ObserversRpc]
    public void UpdateTurretPosition(NetworkObject nob, Vector3 position, Quaternion rotation)
    {
        nob.transform.position = position;
        nob.transform.rotation = rotation;
    }
}
