using UnityEngine;

public class PhysicsSceneSimulateUselessScript : MonoBehaviour
{
    [SerializeField] float simulationSpeed = 1f;

    private void FixedUpdate()
    {
        gameObject.scene.GetPhysicsScene().Simulate(Time.fixedDeltaTime * simulationSpeed);
    }
}
