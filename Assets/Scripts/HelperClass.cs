using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class HelperClass : MonoBehaviour
{
    #region Singleton
    public static HelperClass Instance { get; private set; }

    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(this);
            return;
        }

        Instance = this;
    }

    #endregion

    public enum UnitType { Infantry, Support, AntiArmor, Armored }

    public struct UnitProperties
    {
        public UnitProperties(int _damageMin, int _damageMax, int _hp, int _price, int _movementSpeed)
        {
            hp = _hp;
            damageMin = _damageMin;
            damageMax = _damageMax;
            price = _price;
            movementSpeed = _movementSpeed;
        }

        public int hp;
        public int damageMin;
        public int damageMax;
        public int price;
        public int movementSpeed;
    }

    public enum AttackableType { Unit, Base }

    public static int RaidPrice = 100;

    public static string VirtualMainCameraTag = "VirtualMainCamera";
}