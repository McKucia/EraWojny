using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckBoxOverlap : MonoBehaviour
{
    [SerializeField] LayerMask layerMask;

    public int HowManyColliders()
    {
        Collider[] hitColliders
            = Physics.OverlapBox(gameObject.transform.position, transform.localScale / 2, Quaternion.identity, layerMask);

        return hitColliders.Length;
    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireCube(transform.position, transform.localScale);
    }
}
