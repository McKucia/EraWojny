using FishNet.Object;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;
using UnityEngine.UI;

public class MainCanvas : MonoBehaviour
{
    [SerializeField] Drag turretDrag;
    [SerializeField] Image turretShadowImage;
    [SerializeField] GameObject upgradesPopup;

    [SerializeField] Image raidImage;
    [SerializeField] Image raidShadowImage;

    [SerializeField] Image infantryImage;
    [SerializeField] Image supportImage;
    [SerializeField] Image antiArmorImage;
    [SerializeField] Image armoredImage;
    [SerializeField] Image infantryShadowImage;
    [SerializeField] Image supportShadowImage;
    [SerializeField] Image antiArmorShadowImage;
    [SerializeField] Image armoredShadowImage;

    [SerializeField] List<NetworkObject> turretPrefabs;
    [SerializeField] List<Sprite> turretSprites;
    [SerializeField] List<Sprite> raidSprites;
    public List<Sprite> InfantrySprites;
    public List<Sprite> SupportSprites;
    public List<Sprite> AntiArmorSprites;
    public List<Sprite> ArmoredSprites;
    public CameraMovement CameraMovement;

    [SerializeField] Slider leftSlider;
    [SerializeField] TextMeshProUGUI leftSliderValue;
    [SerializeField] TextMeshProUGUI leftSliderMaxValue;
    [SerializeField] TextMeshProUGUI leftSliderText;

    [SerializeField] Slider rightSlider;
    [SerializeField] TextMeshProUGUI rightSliderValue;
    [SerializeField] TextMeshProUGUI rightSliderMaxValue;
    [SerializeField] TextMeshProUGUI rightSliderText;

    Slider myHealthSlider;
    TextMeshProUGUI myHealthSliderValue;
    TextMeshProUGUI myHealthSliderMaxValue;
    TextMeshProUGUI myHealthSliderText;

    Slider enemyHealthSlider;
    TextMeshProUGUI enemyHealthSliderValue;
    TextMeshProUGUI enemyHealthSliderMaxValue;
    TextMeshProUGUI enemyHealthSliderText;

    Volume globalVolume;

    public void Init(bool inverted)
    {
        gameObject.SetActive(true);
        globalVolume = GameObject.FindGameObjectWithTag("GlobalVolume").GetComponent<Volume>();

        myHealthSlider = inverted ? rightSlider : leftSlider;
        myHealthSliderValue = inverted ? rightSliderValue : leftSliderValue;
        myHealthSliderMaxValue = inverted ? rightSliderMaxValue : leftSliderMaxValue;
        myHealthSliderText = inverted ? rightSliderText : leftSliderText;

        enemyHealthSlider = inverted ? leftSlider : rightSlider;
        enemyHealthSliderValue = inverted ? leftSliderValue : rightSliderValue;
        enemyHealthSliderMaxValue = inverted ? leftSliderMaxValue : rightSliderMaxValue;
        enemyHealthSliderText = inverted ? leftSliderText : rightSliderText;
    }

    public void ShowUpgradesPopup()
    {
        CameraMovement.CanMove = false;

        if (globalVolume.sharedProfile.TryGet<DepthOfField>(out var depthOfField))
            depthOfField.active = true;
        upgradesPopup.SetActive(true);
    }

    public void HideUpgradesPopup()
    {
        CameraMovement.CanMove = true;
        if (globalVolume.sharedProfile.TryGet<DepthOfField>(out var depthOfField))
            depthOfField.active = false;

        upgradesPopup.SetActive(false);
    }

    public void InitHealthSlider(float value, bool isMine)
    {
        myHealthSlider.maxValue = value;
        myHealthSlider.value = value;
        myHealthSliderValue.text = value.ToString();
        myHealthSliderMaxValue.text = value.ToString();
        myHealthSliderText.text = "YOUR BASE HEALTH";

        enemyHealthSlider.maxValue = value;
        enemyHealthSlider.value = value;
        enemyHealthSliderValue.text = value.ToString();
        enemyHealthSliderMaxValue.text = value.ToString();
        enemyHealthSliderText.text = "ENEMY BASE HEALTH";
    }

    public void UpdateHealthSlider(float value, bool isMine)
    {

        if (isMine)
        {
            myHealthSlider.value = value;
            myHealthSliderValue.text = value.ToString();
        }
        else
        {
            enemyHealthSlider.value = value;
            enemyHealthSliderValue.text = value.ToString();
        }
    }

    public void Upgrade(int currentBaseLevel)
    {
        turretDrag.image.sprite = turretSprites[currentBaseLevel];
        turretShadowImage.sprite = turretSprites[currentBaseLevel];

        raidImage.sprite = raidSprites[currentBaseLevel];
        raidShadowImage.sprite = raidSprites[currentBaseLevel];

        infantryImage.sprite  = InfantrySprites[currentBaseLevel];
        supportImage.sprite   = SupportSprites[currentBaseLevel];
        antiArmorImage.sprite = AntiArmorSprites[currentBaseLevel];
        armoredImage.sprite   = ArmoredSprites[currentBaseLevel];

        infantryShadowImage.sprite  = InfantrySprites[currentBaseLevel];
        supportShadowImage.sprite   = SupportSprites[currentBaseLevel];
        antiArmorShadowImage.sprite = AntiArmorSprites[currentBaseLevel];
        armoredShadowImage.sprite   = ArmoredSprites[currentBaseLevel];
    }
}
