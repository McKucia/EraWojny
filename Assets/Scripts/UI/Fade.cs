using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

public class Transitions : MonoBehaviour
{
    [SerializeField] bool simultaneously;
    [SerializeField] Image fade;
    [SerializeField] Transform move;
    [SerializeField] float fadeSpeed;
    [SerializeField] float moveSpeed;
    [SerializeField] float targetYPosition;
    [SerializeField] Transitions otherTransition;
    public bool StartFlag;

    float currentQuantity;
    float desiredQuantity;
    Color c;
    bool processed = false;
    bool moved = false;

    void Start()
    {
        currentQuantity = 1;
        desiredQuantity = 0;
        c = fade.color;
    }

    void Update()
    {
        if (!StartFlag) return;
        if (simultaneously && !moved)
        {
            Process();
            moved = MoveUp();
        }
        else
        {
            if (moved) return;
            processed = Process();
            if (processed)
                moved = MoveUp();
        }
    }

    bool Process()
    {
        if (desiredQuantity >= currentQuantity)
        {
            if (otherTransition)
                otherTransition.StartFlag = true;
            return true;
        }

        currentQuantity = Mathf.MoveTowards(
            currentQuantity,
            desiredQuantity,
            fadeSpeed * Time.deltaTime);

        c.a = currentQuantity;
        fade.color = c;

        return false;
    }

    bool MoveUp()
    {
        var localPosition = move.localPosition;

        if (move.localPosition.y + 1f >= targetYPosition)
            return true;

        float nextPositionY = Mathf.Lerp(move.localPosition.y, targetYPosition, moveSpeed * Time.deltaTime);
        move.localPosition = new Vector3(localPosition.x, nextPositionY, localPosition.z);

        return false;
    }
}
