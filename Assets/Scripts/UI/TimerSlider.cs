using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using static UnityEngine.Rendering.DebugUI;

public class TimerSlider : MonoBehaviour
{
    [SerializeField] Slider timerSlider;

    public void StartTimer(float seconds)
    {
        StartCoroutine(StartTimerTicker(seconds));
    }

    IEnumerator StartTimerTicker(float seconds)
    {
        timerSlider.maxValue = seconds;
        timerSlider.value = seconds;

        while (seconds > 0)
        {
            seconds -= Time.deltaTime;
            yield return new WaitForSecondsRealtime(0.001f);
            timerSlider.value = seconds;
        }
        timerSlider.value = timerSlider.maxValue;
    }
}
