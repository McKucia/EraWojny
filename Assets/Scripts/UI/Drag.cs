using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Drag : MonoBehaviour, IDragHandler, IBeginDragHandler, IEndDragHandler
{
    public Image image;

    [HideInInspector] public Vector3 startPosition;
    [HideInInspector] public Base currentBase;

    public void OnBeginDrag(PointerEventData eventData)
    {
        image.raycastTarget = false;
        currentBase.SetTurretsSlotsActive(true);
    }

    public void OnDrag(PointerEventData eventData)
    {
        transform.position = eventData.position;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        transform.position = startPosition;
        image.raycastTarget = true;
        currentBase.SetTurretsSlotsActive(false);
    }

    void Start()
    {
        image = GetComponent<Image>();
        startPosition = transform.position;
    }
}
