using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SpawnUnitButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerExitHandler
{
    [SerializeField] string message;
    [SerializeField] PlayerManager playerManager;
    [SerializeField] int unitType;
    [SerializeField] float holdTime;

    Button tooltipButton;
    private float timer = 0f;
    private bool isHolding = false;

    void Start()
    {
        tooltipButton = GetComponent<Button>();
    }

    void Update()
    {
        if (isHolding)
        {
            timer += Time.deltaTime;
            if (timer >= holdTime)
            {
                OnButtonHold();
                Reset();
            }
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if(TooltipManager.instance.PlayerManager == null)
            TooltipManager.instance.PlayerManager = playerManager;
        isHolding = true;
        timer = 0f;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        if (isHolding && timer < holdTime)
        {
            OnButtonClick();
        }
        Reset();
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        Reset();
    }

    private void Reset()
    {
        isHolding = false;
        timer = 0f;
    }

    private void OnButtonClick()
    {
        playerManager.SpawnUnit(unitType);
    }

    private void OnButtonHold()
    {
        TooltipManager.instance.SetAndShowTooltip(unitType);
    }
}
