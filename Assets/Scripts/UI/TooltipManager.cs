using TMPro;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;
using UnityEngine.UI;

public class TooltipManager : MonoBehaviour
{
    public static TooltipManager instance;
    [SerializeField] TextMeshProUGUI unitTypeText;
    [SerializeField] TextMeshProUGUI price;
    [SerializeField] TextMeshProUGUI maxDamage;
    [SerializeField] TextMeshProUGUI minDamage;
    [SerializeField] TextMeshProUGUI hp;
    [SerializeField] Image unitImage;

    [HideInInspector] public PlayerManager PlayerManager;
    Volume globalVolume;

    void Awake()
    {
        if (instance != null && instance != this)
            Destroy(this.gameObject);
        else
            instance = this;
    }

    void Start()
    {
        globalVolume = GameObject.FindGameObjectWithTag("GlobalVolume").GetComponent<Volume>();
        Cursor.visible = true;
        gameObject.SetActive(false);
    }

    public void SetAndShowTooltip(int unitType)
    {
        if (globalVolume.sharedProfile.TryGet<DepthOfField>(out var depthOfField))
            depthOfField.active = true;

        switch ((HelperClass.UnitType)unitType)
        {
            case HelperClass.UnitType.Infantry:
                unitImage.sprite = PlayerManager.MainCanvas.InfantrySprites[PlayerManager.GetLevel()];
                unitTypeText.text = "INFANTRY";
                break;
            case HelperClass.UnitType.Support:
                unitImage.sprite = PlayerManager.MainCanvas.SupportSprites[PlayerManager.GetLevel()];
                unitTypeText.text = "SUPPORT";
                break;
            case HelperClass.UnitType.AntiArmor:
                unitImage.sprite = PlayerManager.MainCanvas.AntiArmorSprites[PlayerManager.GetLevel()];
                unitTypeText.text = "ANTI-ARMOR";
                break;
            case HelperClass.UnitType.Armored:
                unitImage.sprite = PlayerManager.MainCanvas.ArmoredSprites[PlayerManager.GetLevel()];
                unitTypeText.text = "ARMORED";
                break;
        }
        var unitsProperties = PlayerManager.UnitsProperties[PlayerManager.GetLevel()][(HelperClass.UnitType)unitType];
        price.text = unitsProperties.price.ToString();
        maxDamage.text = unitsProperties.damageMax.ToString();
        minDamage.text = unitsProperties.damageMin.ToString();
        hp.text = unitsProperties.hp.ToString();

        gameObject.SetActive(true);
    }

    public void HideTooltip()
    {
        if (globalVolume.sharedProfile.TryGet<DepthOfField>(out var depthOfField))
            depthOfField.active = false;

        gameObject.SetActive(false);
        price.text = "";
        maxDamage.text = "";
        minDamage.text = "";
        hp.text = "";
        unitImage.sprite = null;
    }
}
