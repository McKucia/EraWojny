using FishNet.Object;
using System.Collections.Generic;
using UnityEngine;

public class Drop : MonoBehaviour
{
    [SerializeField] List<MeshRenderer> meshRenderers;
    [SerializeField] List<SkinnedMeshRenderer> skinnedMeshRenderers;
    [SerializeField] Base currentBase;
    [SerializeField] NetworkObject turretPrefab;
    [SerializeField] int index;

    List<Material> materials;
    bool materialOpaque = false;

    void Start()
    {
        materials = new();
        foreach (var renderer in meshRenderers)
            materials.Add(renderer.material);
        foreach (var renderer in skinnedMeshRenderers)
            materials.Add(renderer.material);
    }

    void Update()
    {
        Preview();
    }

    public void CheckSpawn()
    {
        Ray ray;
        RaycastHit hit;

        ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out hit))
            if (hit.collider.gameObject == this.gameObject)
                currentBase.SpawnTurret(turretPrefab, index);
    }

    void Preview()
    {
        Ray ray;
        RaycastHit hit;

        ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out hit))
        {
            if (hit.collider.gameObject == this.gameObject)
            {
                if (materialOpaque) return;

                UpdateTransparency(1f);
                materialOpaque = true;
            }
            else
            {
                if (!materialOpaque) return;

                UpdateTransparency(0.35f);
                materialOpaque = false;
            }
        }
    }

    void UpdateTransparency(float alpha)
    {
        foreach (var material in materials)
        {
            Color color = material.color;
            color.a = alpha;
            material.color = color;
        }
    }
}
