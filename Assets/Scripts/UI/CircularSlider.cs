using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CircularSlider : MonoBehaviour
{
    [SerializeField] float duration;
    [SerializeField] Slider slider;

    float timeLeft = 0;
    [HideInInspector] public bool Counting = false;

    void Start()
    {
        slider.maxValue = duration;
        slider.value = 0;
    }

    public void StartCounting()
    {
        Counting = true;
        timeLeft = duration;
    }

    void Update()
    {
        if (!Counting) return;

        if (timeLeft > 0)
        {
            timeLeft -= Time.deltaTime;
            slider.value = timeLeft;
        }
        else
        {
            Counting = false;
            timeLeft = duration;
            slider.value = 0;
        }
    }
}
