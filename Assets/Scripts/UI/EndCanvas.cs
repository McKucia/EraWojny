using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class EndCanvas : MonoBehaviour
{
    [SerializeField] float easeInSpeed;
    [SerializeField] RectTransform panel;
    [SerializeField] TextMeshProUGUI textMeshPro;

    bool show = false;

    public void Show(string text)
    {
        textMeshPro.text = text;
        show = true;
    }

    void Update()
    {
        if (!show) return;

        var localScaleTemp = panel.localScale;
        localScaleTemp.x += easeInSpeed * Time.deltaTime;
        localScaleTemp.y += easeInSpeed * Time.deltaTime;
        panel.localScale = localScaleTemp;

        easeInSpeed += Time.deltaTime * 0.8f;

        var tempColor = textMeshPro.color;
        tempColor.a = Mathf.Clamp01(tempColor.a - easeInSpeed * Time.deltaTime / 2f);
        textMeshPro.color = tempColor;
    }
}
