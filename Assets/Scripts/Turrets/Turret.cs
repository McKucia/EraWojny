using FishNet.Object;
using FishNet.Object.Synchronizing;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class Turret : NetworkBehaviour
{
    [SerializeField] protected string targetTag;
    [SerializeField] protected float cooldown;

    [HideInInspector] public PlayerManager PlayerManager;

    readonly protected SyncVar<Unit> targetUnit = new();
    readonly protected SyncVar<Transform> target = new();

    protected AudioSource audioData;
    protected List<GameObject> inRange = new();
    protected float cooldownTimer;

    public override void OnStartClient()
    {
        base.OnStartClient();
        audioData = GetComponent<AudioSource>();
    }

    void OnTriggerEnter(Collider c)
    { 
        if (!base.IsServerInitialized) return;

        if (!c.gameObject.CompareTag(targetTag)) return;
        if (c.gameObject.GetComponent<NetworkObject>()?.OwnerId == this.OwnerId) return;

        AddTarget(c.gameObject);
    }

    void AddTarget(GameObject newTarget)
    {
        inRange.Add(newTarget);
    }

    protected virtual void Shoot() 
    {
        
    }
}
