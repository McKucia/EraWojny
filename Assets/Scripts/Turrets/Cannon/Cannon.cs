using FishNet.Object;
using System.Linq;
using UnityEngine;
using UnitySceneManager = UnityEngine.SceneManagement.SceneManager;

public class Cannon : Turret
{
    [SerializeField] Transform gun;
    [SerializeField] ParticleSystem explosionParticleSystem;
    [SerializeField] Transform bulletInitialPosition;
    [SerializeField] NetworkObject bulletPrefab;
    [SerializeField] float rotateSpeed;

    void Update()
    {
        if (base.IsServerInitialized)
        {
            LookAtTarget();
            Shoot();
        }
    }

    void LookAtTarget()
    {
        inRange.RemoveAll(t => t == null || t.GetComponent<Unit>().isDying.Value == true);

        if (inRange.Count <= 0) return;

        targetUnit.Value = inRange.First().GetComponent<Unit>();
        target.Value = targetUnit.Value.turretTarget;

        var gunLookPos = target.Value.position - gun.transform.position;
        var gunTargetRotation = Quaternion.LookRotation(gunLookPos);
        gun.transform.rotation = Quaternion.Slerp(gun.transform.rotation, gunTargetRotation, rotateSpeed * Time.deltaTime);

        var lookPos = target.Value.position - transform.position;
        lookPos.y = 0;
        var targetRotation = Quaternion.LookRotation(lookPos);
        transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, rotateSpeed * Time.deltaTime);
    }

    protected override void Shoot()
    {
        cooldownTimer -= Time.deltaTime;
        if (cooldownTimer > 0 || inRange.Count <= 0) return;

        cooldownTimer = cooldown;

        if (!target.Value)
        {
            cooldownTimer = cooldown;
            return;
        }

        NetworkObject nob = Instantiate<NetworkObject>(bulletPrefab, bulletInitialPosition.position, transform.rotation);
        UnitySceneManager.MoveGameObjectToScene(nob.gameObject, gameObject.scene);
        nob.GetComponent<Bullet>().Init(target.Value.position, targetUnit.Value, PlayerManager);
        ServerManager.Spawn(nob, base.Owner);
        SetSpawnedObject(nob);
    }

    [ObserversRpc]
    public void SetSpawnedObject(NetworkObject spawned)
    {
        UnitySceneManager.MoveGameObjectToScene(spawned.gameObject, gameObject.scene);
        explosionParticleSystem.Play();
        audioData.Play(0);
    }
}
