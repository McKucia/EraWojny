using FishNet.Object;
using UnityEngine;

public class Bullet : NetworkBehaviour
{
    [SerializeField] float speed;
    [SerializeField] int damage;
    [SerializeField] string targetTag;

    PlayerManager playerManager;
    float despawnTimer = 0.25f;
    Vector3 targetDirection;
    Vector3 target;
    Unit targetUnit;

    public override void OnStartClient()
    {
        base.OnStartClient();
    }

    public void Init(Vector3 targetPosition, Unit aimUnit, PlayerManager manager)
    {
        target = targetPosition;
        targetUnit = aimUnit;
        targetDirection = targetPosition - transform.position;
        playerManager = manager;
    }

    void Update()
    {
        if (!base.IsServerInitialized) return;

        MoveBullet();
    }

    void MoveBullet()
    {
        despawnTimer -= Time.deltaTime;

        transform.position += targetDirection * Time.deltaTime * speed;
        // TODO on trigger enter
        if (Vector3.Distance(transform.position, target) <= 0.1f || despawnTimer <= 0) // TODO fix despawn ...
            TakeDamageToUnit(targetUnit);
    }

    void TakeDamageToUnit(Unit unit)
    {
        gameObject.SetActive(false);

        if (unit.hp.Value - damage <= 0)
            playerManager.AddGold(base.OwnerId, 100);

        unit.TakeDamage(damage, true);
        Despawn(gameObject);
    }
}
