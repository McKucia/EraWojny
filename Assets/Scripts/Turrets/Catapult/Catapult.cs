using FishNet.Component.Animating;
using FishNet.Object;
using System.Linq;
using UnityEngine;

public class Catapult : Turret
{
    [SerializeField] Animator animator;
    [SerializeField] NetworkAnimator networkAnimator;
    [SerializeField] GameObject bulletRoot;
    [SerializeField] NetworkObject bulletPrefab;

    CatapultBullet bullet;

    public override void OnStartClient()
    {
        base.OnStartClient();
    }

    void Update()
    {
        if (base.IsServerInitialized)
        {
            Shoot();
        }
    }

    protected override void Shoot()
    {
        if (!base.IsServerInitialized) return;

        cooldownTimer -= Time.deltaTime;
        if (cooldownTimer > 0 || inRange.Count <= 0) return;

        cooldownTimer = cooldown;

        targetUnit.Value = inRange.First().GetComponent<Unit>();
        target.Value = targetUnit.Value.turretTarget;

        if (!target.Value)
        {
            cooldownTimer = cooldown;
            return;
        }

        networkAnimator.SetTrigger("Shoot");
    }

    public void LoadBullet()
    {
        if(targetUnit.Value == null)
        {
            targetUnit.Value = inRange.Last().GetComponent<Unit>();
            target.Value = targetUnit.Value.turretTarget;
            if (!target.Value)
            {
                cooldownTimer = cooldown;
                return;
            }
        }
        if (base.IsServerInitialized)
        {
            NetworkObject nob = Instantiate<NetworkObject>(bulletPrefab, bulletRoot.transform.position, transform.rotation);
        
            bullet = nob.GetComponent<CatapultBullet>();
        
            bullet.Init(bulletRoot, targetUnit.Value.turretTarget.position, PlayerManager);
        
            ServerManager.Spawn(nob, base.Owner, gameObject.scene);
        
            SetSpawnedObject(nob);
        }
        else
        {
            GameObject nob = Instantiate(bulletPrefab.gameObject, bulletRoot.transform);
            bullet = nob.GetComponent<CatapultBullet>();
        
            bullet.Init(bulletRoot, targetUnit.Value.turretTarget.position, PlayerManager);
        }

    }

    public void ShootBullet()
    {
        bullet.transform.parent = null;
        bullet.Shoot();
    }

    [ObserversRpc]
    public void SetSpawnedObject(NetworkObject spawned)
    {
        if(!base.IsServerInitialized)
            spawned.gameObject.SetActive(false);
    }
}
