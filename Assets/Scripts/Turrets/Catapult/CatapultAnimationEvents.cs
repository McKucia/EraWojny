using FishNet.Managing.Server;
using FishNet.Object;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CatapultAnimationEvents : MonoBehaviour
{
    [SerializeField] Catapult catapult;

    // animation event
    public void LoadBullet()
    {
        catapult.LoadBullet();
    }

    // animation event
    public void ShootBullet()
    {
        catapult.ShootBullet();
    }
}
