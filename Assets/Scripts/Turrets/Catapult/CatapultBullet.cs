using FishNet.Object;
using UnityEngine;

public class CatapultBullet : NetworkBehaviour
{
    [SerializeField] float h;
    [SerializeField] int damage;
    [SerializeField] string targetTag;
    [SerializeField] Rigidbody rb;
    [SerializeField] Vector3 target;
    [SerializeField] float despawnTimer;

    PlayerManager playerManager;
    GameObject bulletRoot;
    float gravity = -20f;
    bool fired = false;

    public override void OnStartClient()
    {
        base.OnStartClient();
    }

    public void Init(GameObject root, Vector3 targetUnit, PlayerManager manager)
    {
        rb.useGravity = false;
        playerManager = manager;
        bulletRoot = root;
        target = targetUnit;
    }

    public void Shoot()
    {
        fired = true;
        Physics.gravity = Vector3.up * gravity;
        rb.useGravity = true;
        rb.velocity = CalculateLaunchVelocity();
    }

    void Update()
    {
        FollowCatapult();
    }

    void FollowCatapult()
    {
        if (fired)
        {
            CheckDespawn();
            return;
        };

        transform.position = bulletRoot.transform.position;
    }

    void CheckDespawn()
    {
        despawnTimer -= Time.deltaTime;

        if (despawnTimer <= 0)
        {
            if (base.IsServerInitialized)
                Despawn(gameObject);
            else
                Destroy(gameObject);
        }
    }

    void OnTriggerEnter(Collider c)
    {
        if (!c.gameObject.CompareTag(targetTag)) return;
        if (c.gameObject.GetComponent<NetworkObject>()?.OwnerId == this.OwnerId) return;

        if (base.IsServerInitialized)
        {
            TakeDamageToUnit(c.gameObject.GetComponent<Unit>());
            Despawn(gameObject);
        }
        else
            Destroy(gameObject);
    }

    void TakeDamageToUnit(Unit unit)
    {
        if (unit.hp.Value - damage <= 0)
            playerManager.AddGold(base.OwnerId, 100);

        unit.TakeDamage(damage, true);
        unit.DisplayDamage(damage);
    }

    Vector3 CalculateLaunchVelocity()
    {
        float displacementY = target.y - rb.position.y;

        Vector3 displacementXZ = 
            new(target.x - rb.position.x, 0, target.z - rb.position.z);

        Vector3 velocityY = Vector3.up * Mathf.Sqrt(-2 * gravity * h);
        Vector3 velocityXZ = 
            displacementXZ / (Mathf.Sqrt(-2 * h / gravity) + Mathf.Sqrt(2 * (displacementY - h) / gravity));

        return velocityXZ + velocityY;
    }
}
