using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    [SerializeField] float cameraBound;
    [SerializeField] Vector3 cameraFollowPosition;
    [SerializeField] Transform virtualCamera;
    [SerializeField] float startRotation;
    [SerializeField] float startPositionX;

    [HideInInspector] public bool CanMove = true;

    const float maxSpeed = 80f;
    const float accelSpeed = 10f;
    float scrollSpeed = 0;
    bool initialMove = false;

    public void Init()
    {
        gameObject.SetActive(true);
        virtualCamera.gameObject.SetActive(true);
        virtualCamera.rotation = Quaternion.identity;
        virtualCamera.transform.Rotate(startRotation, 0, 0);
        virtualCamera.transform.position = new Vector3(startPositionX, 0, 0);
    }

    void Update()
    {   
        if(CanMove)
            UpdateCameraPosition();
    }

    void UpdateCameraPosition()
    {
        if ((Input.GetMouseButton(0) && !MouseOverUILayerObject.IsPointerOverUIObject()) || !initialMove)
        {
            initialMove = true;
            float screenEdge = Screen.width / 4f;

            scrollSpeed = Mathf.SmoothStep(scrollSpeed, maxSpeed, Time.deltaTime * accelSpeed);

            if (Input.mousePosition.x > Screen.width - screenEdge)
                cameraFollowPosition.x += scrollSpeed * Time.deltaTime;
            else if (Input.mousePosition.x < screenEdge)
                cameraFollowPosition.x -= scrollSpeed * Time.deltaTime;
            else
                scrollSpeed = 0f;

            cameraFollowPosition.x = Mathf.Clamp(cameraFollowPosition.x, -cameraBound, cameraBound);
            virtualCamera.position = cameraFollowPosition;
        }
        else
            scrollSpeed = 0f;
    }
}
