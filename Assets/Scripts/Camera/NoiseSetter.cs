using Cinemachine;
using UnityEngine;

public class NoiseSetter : MonoBehaviour
{
    float timeRemaining;
    bool counting = false;
    CinemachineBasicMultiChannelPerlin perlin;

    void Start()
    {
        perlin = GetComponent<CinemachineVirtualCamera>()
            .GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>();
    }

    void Update()
    {
        if (!counting) return;
        timeRemaining -= Time.deltaTime;

        if (timeRemaining <= 0)
        {
            counting = false;
            ActivateNoise(0, 0);
        }
    }

    public void SetNoise(float duration, float noiseAmplitude, float noiseFrequency)
    {
        counting = true;
        timeRemaining = duration;
        ActivateNoise(noiseAmplitude, noiseFrequency);
    }

    void ActivateNoise(float noiseAmplitude, float noiseFrequency)
    {
        perlin.m_AmplitudeGain = noiseAmplitude;
        perlin.m_FrequencyGain = noiseFrequency;
    }
}
