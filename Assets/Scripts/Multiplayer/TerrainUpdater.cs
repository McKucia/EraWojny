using FishNet.Object;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerrainUpdater : NetworkBehaviour
{    
    [SerializeField] string ageTerrainTag;
    [SerializeField] GameObject[] terrains;

    const int teleportDistance = 1000;
    int currentAge = 0;

    public static TerrainUpdater Instance { get; private set; }

    void Awake()
    {
        Instance = this;
    }

    public override void OnStartClient()
    {
        base.OnStartClient();

        terrains = GameObject.FindGameObjectsWithTag(ageTerrainTag);
        Array.Sort(terrains, delegate (GameObject x, GameObject y) { return x.name.CompareTo(y.name); });


        for (int i = 0; i < terrains.Length; i++)
            if (i != currentAge)
                terrains[i].transform.Translate(new Vector3(0, teleportDistance, 0));
    }

    public void UpdateTerrain(int index)
    {
        if (index > currentAge)
        {
            terrains[index - 1].transform.Translate(new Vector3(0, teleportDistance, 0));
            terrains[index].transform.Translate(new Vector3(0, -teleportDistance, 0));
            currentAge = index;
        }
    }
}
