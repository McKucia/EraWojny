using FishNet.Managing;
using FishNet.Transporting.UTP;
using System.Threading.Tasks;
using Unity.Networking.Transport.Relay;
using Unity.Services.Relay;
using Unity.Services.Relay.Models;
using UnityEngine;

public class RelayManager : MonoBehaviour
{
    [SerializeField] NetworkManager networkManager;

    public static RelayManager Instance { get; private set; }

    private void Awake()
    {
        if (Instance != null && Instance != this)
            Destroy(this);
        else
            Instance = this;
    }

    public async Task<string> CreateRelay()
    {
        try
        {
            Allocation allocation = await RelayService.Instance.CreateAllocationAsync(1);

            string joinCode = await RelayService.Instance.GetJoinCodeAsync(allocation.AllocationId);

            var unityTransport = networkManager.TransportManager.GetTransport<FishyUnityTransport>();
            unityTransport.SetRelayServerData(new RelayServerData(allocation, "dtls"));

            if (networkManager.ServerManager.StartConnection())
                networkManager.ClientManager.StartConnection();

            return joinCode;
        }
        catch(RelayServiceException ex)
        {
            Debug.Log(ex);
            return null;
        }
    }

    public async void JoinRelay(string joinCode)
    {
        try
        {
            var joinAllocation = await RelayService.Instance.JoinAllocationAsync(joinCode);

            var unityTransport = networkManager.TransportManager.GetTransport<FishyUnityTransport>();

            unityTransport.SetRelayServerData(new RelayServerData(joinAllocation, "dtls"));

            networkManager.ClientManager.StartConnection();
        }
        catch (RelayServiceException ex)
        {
            Debug.Log(ex);
        }
    }
}
