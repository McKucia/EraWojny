using FishNet;
using FishNet.Object;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UIElements;

public class PlayerSpawner : NetworkBehaviour
{
    [SerializeField] NetworkObject playerPrefab;
    [SerializeField] List<Transform> spawnPoints;
    public static PlayerSpawner Instance { get; private set; }

    void Awake()
    {
        Instance = this;
    }

    public void SpawnPlayers()
    {
        if (!base.IsServerInitialized) return;

        foreach(var c in InstanceFinder.ClientManager.Clients)
        {
            var spawnPoint = spawnPoints.First(); 
            NetworkObject nob = Instantiate<NetworkObject>(playerPrefab, spawnPoint.position, spawnPoint.rotation);
            ServerManager.Spawn(nob, c.Value);
            spawnPoints.Remove(spawnPoint);
        }
    }
}
