using FishNet;
using FishNet.Managing.Scened;
using FishNet.Object;
using UnityEngine;

public class SceneLoader : MonoBehaviour
{
    [SerializeField] string gameSceneName;

    const int targetPlayersCount = 2; // TODO: change to 2
    int playersCount = 0;

    public static SceneLoader Instance { get; private set; }

    void Awake()
    {
        Instance = this;
    }

    void Start()
    {
        InstanceFinder.SceneManager.OnClientPresenceChangeEnd += ClientLoadedScene;

    }

    public void LoadScene()
    {
        SceneLoadData sld = new(gameSceneName);
        sld.ReplaceScenes = ReplaceOption.All;
        InstanceFinder.SceneManager.LoadGlobalScenes(sld);
    }

    void ClientLoadedScene(ClientPresenceChangeEventArgs obj)
    {
        playersCount++;
        if(playersCount == targetPlayersCount) // TODO: change >=
        {
            PlayerSpawner.Instance.SpawnPlayers();
        }
    }
}
