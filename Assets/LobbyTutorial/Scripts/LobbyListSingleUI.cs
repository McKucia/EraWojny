using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Unity.Services.Lobbies.Models;
using UnityEngine.UI;
using System.Linq;

public class LobbyListSingleUI : MonoBehaviour {

    
    [SerializeField] private TextMeshProUGUI lobbyNameText;
    [SerializeField] private TextMeshProUGUI playersText;

    private Lobby lobby;

    public void JoinLobby()
    {
        LobbyManager.Instance.JoinLobby(lobby);
    }

    public void UpdateLobby(Lobby lobby) {
        this.lobby = lobby;

        lobbyNameText.text = lobby.Name;
        playersText.text = lobby.Players.Where(p => p.Id == lobby.HostId).First().Data[LobbyManager.KEY_PLAYER_NAME].Value;
    }


}