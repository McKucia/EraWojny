using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class EditPlayerName : MonoBehaviour
{
    public static EditPlayerName Instance { get; private set; }
    [SerializeField] private TMP_InputField playerNameInputField;


    private string playerName;


    private void Awake()
    {
        Instance = this;
    }

    public string GetPlayerName() {
        return playerNameInputField.text;
    }


}