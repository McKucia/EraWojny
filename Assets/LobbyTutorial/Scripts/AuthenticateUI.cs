using TMPro;
using UnityEngine;
using UnityEngine.UI;
using WebSocketSharp;

public class AuthenticateUI : MonoBehaviour {

    [SerializeField] GameObject nicknameUI;
    [SerializeField] GameObject logo;
    [SerializeField] TextMeshProUGUI errorMessage;
    [SerializeField] TMP_InputField editPlayerName;
    [SerializeField] Button authenticateButton;
    [SerializeField] GameObject loadingBar;
    [SerializeField] GameObject playIcon;

    public async void ValidateName()
    {
        var name = EditPlayerName.Instance.GetPlayerName();
        if (name.IsNullOrEmpty())
        {
            errorMessage.text = "Please enter your nickname.";
            return;
        }
        if (name.Length < 3)
        {
            errorMessage.text = "Your name should contain at least 3 characters.";
            return;
        }
        errorMessage.text = "";
        editPlayerName.interactable = false;
        authenticateButton.interactable = false;
        playIcon.SetActive(false);
        loadingBar.SetActive(true);

        await LobbyManager.Instance.Authenticate(name);

        Hide();
    }

    void Hide() {
        nicknameUI.SetActive(false);
        logo.SetActive(false);
    }

}