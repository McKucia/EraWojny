using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class LobbyCreateUI : MonoBehaviour 
{
    public static LobbyCreateUI Instance { get; private set; }

    [SerializeField] TextMeshProUGUI errorMessage;
    [SerializeField] GameObject loadingBar;
    [SerializeField] private Button createButton;
    [SerializeField] private Button closeButton;
    [SerializeField] private TextMeshProUGUI createButtonText;
    [SerializeField] private Button publicPrivateButton;
    [SerializeField] private TMP_InputField lobbyNameText;
    [SerializeField] private TextMeshProUGUI publicPrivateText;

    private bool isPrivate;

    private void Awake() {
        Instance = this;

        publicPrivateButton.onClick.AddListener(() => {
            isPrivate = !isPrivate;
            UpdateText();
        });

        Hide();
    }

    public async void ValidateName()
    {
        if (lobbyNameText.text.Length <= 0)
        {
            errorMessage.text = "Please enter lobby name.";
            return;
        }
        if (lobbyNameText.text.Length < 3)
        {
            errorMessage.text = "Lobby name should contain at least 3 characters.";
            return;
        }
        errorMessage.text = "";
        lobbyNameText.interactable = false;
        createButton.interactable = false;
        publicPrivateButton.interactable = false;
        createButtonText.text = "";
        closeButton.interactable = false;
        loadingBar.SetActive(true);

        await LobbyManager.Instance.CreateLobby(
                lobbyNameText.text,
                isPrivate
            );

        lobbyNameText.interactable = true;
        createButton.interactable = true;
        publicPrivateButton.interactable = true;
        createButtonText.text = "CREATE";
        lobbyNameText.text = "";

        closeButton.interactable = true;
        loadingBar.SetActive(false);

        Hide();
    }

    private void UpdateText() {
        publicPrivateText.text = isPrivate ? "Private" : "Public";
    }

    public void Hide() {
        gameObject.SetActive(false);
    }

    public void Show() {
        gameObject.SetActive(true);
        isPrivate = false;

        UpdateText();
    }

}